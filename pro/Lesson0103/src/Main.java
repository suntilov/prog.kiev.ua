/**
 1. Создать аннотацию, которая принимает параметры для метода. Написать код, который вызовет метод,
 помеченный этой аннотацией, и передаст параметры аннотации в вызываемый метод.
 @Test(a=2, b=5)
 public void test(int a, int b) {…}
 */

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        final Class<?> cls = SomeClass.class;

        Method[] methods = cls.getMethods();
        for (Method method : methods)
            if (method.isAnnotationPresent(Test.class)) {
                Test an = method.getAnnotation(Test.class);
                SomeClass o = null;
                try {

                    try {
                        o = (SomeClass)cls.newInstance();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    }

                    method.invoke(o, an.param1(), an.param2());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
    }
}
