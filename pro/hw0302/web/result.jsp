<html>
  <head>
    <title>Prog.kiev.ua</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </head>
  <body>
  	<%String javaPercent = (String)session.getAttribute("javaPercent"); %>
	<%String dotNetPercent = (String)session.getAttribute("dotNetPercent"); %>

	  <div class="container">
  		<h1>Result</h1>
  		<h2>Java</h2>
		<div class="progress">
			<div class="progress-bar" role="progressbar"
				 aria-valuenow="<%=javaPercent%>" aria-valuemin="0" aria-valuemax="100"
				 style="width:<%=javaPercent%>%">
				<%=javaPercent%>%
			</div>
		</div>
		<h2>.Net</h2>
		<div class="progress">
			<div class="progress-bar" role="progressbar"
				 aria-valuenow="<%=dotNetPercent%>" aria-valuemin="0" aria-valuemax="100"
				 style="width:<%=dotNetPercent%>%">
				<%=dotNetPercent%>%
			</div>
		</div>
		<a href="\" class="btn btn-info" role="button">Back</a>
	</div>
  </body>
</html>
