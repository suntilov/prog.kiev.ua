/**
 Создать проект «Анкета». Сделать возможность
 ввода пользователем его имени, фамилии,
 возраста и ответов на 2-3 вопроса. Данные
 должны отправляться на сервер, который в ответ
 должен вернуть статистику по ответам в виде
 HTML документа.
 */

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@SuppressWarnings("serial")
public class QuestionServlet extends HttpServlet {

    private int java;
    private int dotNet;

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        java += getAnswer(req.getParameter("question1"));
        dotNet += getAnswer(req.getParameter("question2"));

        int javaPercent = java + dotNet > 0 ? java * 100 / (java + dotNet) : 0;
        int dotNetPercent = java + dotNet > 0 ? 100 - javaPercent : 0;

        HttpSession session = req.getSession(true);
        session.setAttribute("javaPercent", "" + javaPercent);
        session.setAttribute("dotNetPercent", "" + dotNetPercent);

        resp.sendRedirect("result.jsp");
    }

    private int getAnswer(String answer) {
        return answer.equals("yes") ? 1 : 0;
    }

}
