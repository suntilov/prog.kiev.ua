import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "trains")
public class Schedule {

    private String name;

    @XmlElement(name="train")
    private List<Train> trains = new ArrayList<Train>();

    public Schedule() {
    }

    public Schedule(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void add(Train train) {
        trains.add(train);
    }

    public void remove(Train train) {
        trains.remove(train);
    }

    public Schedule find(Date date, Date from, Date to) {
        Schedule found = new Schedule();
        for (Train train : trains) {
            if (date.compareTo(train.getDate()) != 0)
                continue;
            if (from.compareTo(train.getDeparture()) > 0)
                continue;
            if (to.compareTo(train.getDeparture()) < 0)
                continue;
            found.add(train);
        }
        return found;
    }

    @Override
    public String toString() {
        return "Schedule " + name + " {" +
                "trains=" + trains +
                '}';
    }
}
