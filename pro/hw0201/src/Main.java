/**
 Есть список поездов, представленный с виде XML. Вывести на экран информацию о тех поездах, которые
 отправляются сегодня с 15:00 до 19:00.
 <?xml version="1.0" encoding="UTF-8"?>
 <trains>
 <train id=“1”>
 <from>Kyiv</from>
 <to>Donetsk</to>
 <date>19.12.2013</date>
 <departure>15:05</departure>
 </train>
 <train id=“2”>
 <from>Lviv</from>
 <to>Donetsk</to>
 <date>19.12.2013</date>
 <departure>19:05</departure>
 </train>
 </trains>
 Написать код для добавления новых поездов в существующий XML.
 */

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Date;
import java.util.StringJoiner;

public class Main {
    public static void main(String[] args) {

        try {
            File file = new File("schedule.xml");
            JAXBContext jaxbContext = null;

            jaxbContext = JAXBContext.newInstance(Schedule.class);

            // читаем из файла
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Schedule schedule = (Schedule) unmarshaller.unmarshal(file);
            schedule.setName("main");
            System.out.println(schedule);

            DateAdapter da = new DateAdapter();
            TimeAdapter ta = new TimeAdapter();

            // Добавляем поезд в расписание
            Train train = new Train("3", "Kiev", "Odessa", da.unmarshal("11.12.2016"), ta.unmarshal("12:44") );
            schedule.add(train);

            // Пишем в файл
            Marshaller marshaller = jaxbContext.createMarshaller();
            // читабельное форматирование
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(schedule, file);

            // ищем поезда
            Date date = da.unmarshal("19.12.2013");
            Schedule found = schedule.find(
                    date,
                    ta.unmarshal("12:00"),
                    ta.unmarshal("18:00"));
            found.setName("found on " + date);
            System.out.println(found);

        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
