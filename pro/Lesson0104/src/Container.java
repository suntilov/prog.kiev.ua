import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

@SaveTo(path = "C:\\tmp\\file.txt")
public class Container {
    String text = "some text";

    @Saver
    public void save(String fileName) {
        try (PrintWriter file = new PrintWriter(fileName)) {
            file.print(text);
        }
        catch (FileNotFoundException e) {
            System.out.println("Error file white to " + fileName);
        }
    }
}
