import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

public class TextSaver {

    public void save(Object obj) {
        final Class<?> cls = obj.getClass();
        SaveTo an = cls.getAnnotation(SaveTo.class);
        Method[] methods = cls.getMethods();
        for (Method method : methods)
            if (method.isAnnotationPresent(Saver.class)) {
                    try {
                        method.invoke(obj, an.path());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
        }

}
