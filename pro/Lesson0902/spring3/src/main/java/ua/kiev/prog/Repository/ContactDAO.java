package ua.kiev.prog.Repository;

import ua.kiev.prog.Entity.Contact;
import ua.kiev.prog.Entity.Group;

import java.util.List;

public interface ContactDAO {
    void add(Contact contact);
    void delete(Contact contact);
    void delete(long[] ids);
    List<Contact> list(Group group, int page, int pageSize);
    List<Contact> list(String pattern, int page, int pageSize);
    int count();
}
