package ua.kiev.prog.Repository;

import org.springframework.stereotype.Repository;
import ua.kiev.prog.Entity.Contact;
import ua.kiev.prog.Entity.Group;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository  // не нужно описывать been
public class ContactDAOImpl implements ContactDAO {

    @PersistenceContext  // работает как @Autowired
    private EntityManager entityManager;

    @Override
    public void add(Contact contact) {
        entityManager.merge(contact);
    }

    @Override
    public void delete(Contact contact) {
        entityManager.remove(contact);
    }

    @Override
    public void delete(long[] ids) {
        Contact c;
        for (long id : ids) {
            c = entityManager.getReference(Contact.class, id);
            entityManager.remove(c);
        }
    }

    @Override
    public List<Contact> list(Group group, int page, int pageSize) {
        Query query;
        String statement;
        if (group != null) {
            statement = "SELECT c FROM Contact c WHERE c.group = :group" + pageSizeStatement(page, pageSize);
            query = entityManager.createQuery(statement, Contact.class);
            query.setParameter("group", group);
        } else {
            statement = "SELECT c FROM Contact c" + pageSizeStatement(page, pageSize);
            query = entityManager.createQuery(statement, Contact.class);
        }

        return (List<Contact>) query.getResultList();
    }

    @Override
    public List<Contact> list(String pattern, int page, int pageSize) {
        String statement = "SELECT c FROM Contact c WHERE c.name LIKE :pattern" + pageSizeStatement(page, pageSize);
        Query query = entityManager.createQuery(statement, Contact.class);
        query.setParameter("pattern", "%" + pattern + "%");
        return (List<Contact>) query.getResultList();
    }

    @Override
    public int count() {
        Query query = entityManager.createQuery("SELECT count(*) FROM Contact c", Contact.class);
        return query.getFirstResult();
    }

    private String pageSizeStatement(int page, int pageSize) {
        if (pageSize > 0)
            return " limit " + page * pageSize + ", " + pageSize;
        else
            return "";
    }

}