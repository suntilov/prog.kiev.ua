package com.company.command;

public interface Command {

    boolean execute();
    void printResult();
}
