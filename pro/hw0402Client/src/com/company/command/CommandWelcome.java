package com.company.command;

public class CommandWelcome implements Command {

    @Override
    public boolean execute() {
        return true;
    }

    @Override
    public void printResult() {
        System.out.println(
                "Command starts with #\n" +
                        "Available commands:\n" +
                        " #help\n" +
                        " #commands\n" +
                        " #userlist\n" +
                        " #private:user\n" +
                        " #exitprivate\n" +
                        " #roomlist\n" +
                        " #room:name\n" +
                        " #exitroom\n" +
                        "empty message for exit.\n" +
                        "Enter your message or command:\n");
    }
}
