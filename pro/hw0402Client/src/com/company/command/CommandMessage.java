package com.company.command;

import com.company.User;
import com.company.Utils;
import com.company.message.Message;

import java.io.IOException;

public class CommandMessage implements Command {
    private Message message;
    int res;

    public CommandMessage(User user, String text) {
        message = new Message(user.getLogin(), user.getPrivateTo(), text);
        res = 200;
    }

    @Override
    public boolean execute() {
        try {
            res = message.send(Utils.getURL());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res == 200 ? true : false;
    }

    @Override
    public void printResult() {
        if (res != 200)
            System.out.println("HTTP error occured: " + res);
    }
}
