package com.company.command;

import com.company.Utils;
import com.company.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static com.company.Utils.requestBodyToArray;

public class CommandUserList implements Command {
    private final Gson gson;
    JsonUserList list;
    int res;

    public CommandUserList() {
        gson = new GsonBuilder().create();
        res = 200;
    }

    @Override
    public boolean execute() {
        URL url = null;
        try {
            url = new URL(Utils.getURL() + "/getuserlist");

            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            http.setRequestMethod("GET");
            http.setDoOutput(true);

            InputStream is = http.getInputStream();
            try {
                byte[] buf = requestBodyToArray(is);
                String strBuf = new String(buf, StandardCharsets.UTF_8);

                list = gson.fromJson(strBuf, JsonUserList.class);
            } finally {
                is.close();
            }

            res = http.getResponseCode();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return res == 200 ? true : false;
    }

    @Override
    public void printResult() {
        if (list != null) {
            for (User user : list.getList()) {
                System.out.println(user);
            }
        }
    }

    public class JsonUserList {
        private final List<User> list;

        public JsonUserList(List<User> sourceList) {
            this.list = sourceList;
        }

        public List<User> getList() {
            return Collections.unmodifiableList(list);
        }
    }
}
