package com.company.command;

import com.company.User;

public class CommandFactory {
    private User user;

    public CommandFactory(User user) {
        this.user = user;
    }

    public Command getCommand(String text) {

        if (text.startsWith("#welcome"))
            return new CommandWelcome();

        if (text.startsWith("#help"))
            return new CommandWelcome();

        if (text.startsWith("#commands"))
            return new CommandWelcome();

        if (text.startsWith("#userlist"))
            return new CommandUserList();

        if (text.startsWith("#private:"))
            return new CommandPrivate(user, text);

        if (text.startsWith("#exitprivate"))
            return new CommandPrivate(user, ":");

        if (text.startsWith("#roomlist"))
            return new CommandDummy();

        if (text.startsWith("#room"))
            return new CommandDummy();

        if (text.startsWith("#exitroom"))
            return new CommandDummy();

        return new CommandMessage(user, text);
    }
}

