package com.company.command;

public class CommandDummy implements Command {

    @Override
    public boolean execute() {
        return true;
    }

    @Override
    public void printResult() {
        System.out.println("command under construction");
    }
}
