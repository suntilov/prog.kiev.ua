package com.company.command;

import com.company.User;

public class CommandPrivate implements Command {

    private User user;

    public CommandPrivate(User user, String command) {
        String to = getTo(command);
        user.setPrivateTo(to);
        this.user = user;
    }

    @Override
    public boolean execute() {
        return true;
    }

    @Override
    public void printResult() {
        if (user.getPrivateTo().isEmpty())
            System.out.println("Exit private mode");
        else
            System.out.println("Enter private mode to " + user.getPrivateTo());
    }

    private static String getTo(String command) {
        return command.substring(command.indexOf(":", 0) + 1);
    }
}

