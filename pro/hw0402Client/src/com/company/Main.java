/**
 * По чату:
 * 1. Добавить ф-ю авторизации пользователей.
 * 2. Добавить ф-ю приватных сообщений.
 * 3. Добавить ф-ю получения списка всех пользователей.
 * 4. Добавить ф-ю чат-комнат.
 * 5. Добавить ф-ю проверки статуса пользователя.
 */

package com.company;

import com.company.command.Command;
import com.company.command.CommandFactory;
import com.company.message.GetMessageListThread;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Enter your login: ");
            String login = scanner.nextLine();

            User user = new User(login);
            if (user.authorize() != 200) {
                System.out.println("Authorisation failed.");
                return;
            }

            Thread th = new Thread(new GetMessageListThread(user));
            th.setDaemon(true);
            th.start();

            CommandFactory commandFactory = new CommandFactory(user);
            Command command = commandFactory.getCommand("#welcome");
            command.printResult();

            while (true) {

                String text = scanner.nextLine();
                if (text.isEmpty())
                    break;

                command = commandFactory.getCommand(text);
                if (!command.execute())
                    break;
                command.printResult();
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            scanner.close();
        }
    }
}
