package hw0601;

public class Appartment {

    private int id;
    private String district;
    private String address;
    private int m2;
    private int rooms;
    private int price;

    public Appartment() {
    }

    public Appartment(int id, String district, String address, int m2, int rooms, int price) {
        this.id = id;
        this.district = district;
        this.address = address;
        this.m2 = m2;
        this.rooms = rooms;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getM2() {
        return m2;
    }

    public void setM2(int m2) {
        this.m2 = m2;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void find(DbProperties props, int id) {

    }
}
