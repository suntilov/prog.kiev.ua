package hw0601;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DbInit {

    public DbInit() {
    }

    public static void createTables(DbProperties props) {

        try (Connection conn = DriverManager.getConnection(props.getUrl(), props.getUser(), props.getPassword())) {
            try (Statement st = conn.createStatement()) {
                st.execute("DROP TABLE IF EXISTS appartment");
                st.execute("CREATE TABLE `appartmentdb`.`appartment` (\n" +
                        "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                        "  `district` VARCHAR(45) NULL,\n" +
                        "  `address` VARCHAR(45) NULL,\n" +
                        "  `m2` INT NULL,\n" +
                        "  `rooms` VARCHAR(45) NULL,\n" +
                        "  `price` DECIMAL(15,2) NULL,\n" +
                        "  PRIMARY KEY (`id`),\n" +
                        "  UNIQUE INDEX `id_UNIQUE` (`id` ASC));");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
