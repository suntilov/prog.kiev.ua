use mydb;

CREATE TABLE Clients (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
group_id INT DEFAULT NULL,
name VARCHAR(128) DEFAULT NULL,
surname VARCHAR(128) DEFAULT NULL,
email VARCHAR(128) DEFAULT NULL,
phone VARCHAR(12) NOT NULL
);

select * from Clients;

INSERT INTO Clients (phone) VALUES ("0503555999");

INSERT INTO Clients (name, email, phone) VALUES ("Vsevolod", "info@prog.kiev.ua", "0932565148");

INSERT INTO Clients (name, email, phone) VALUES ("Somebody", "smb@prog.kiev.ua", "0442426548");

UPDATE Clients SET email = "test@prog.kiev.ua", phone="0501234567" WHERE id = 3;

CREATE TABLE Groups (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(128) NOT NULL,
comment TEXT DEFAULT NULL,
date DATE DEFAULT NULL,
start TIME DEFAULT NULL,
end TIME DEFAULT NULL
);

INSERT INTO Groups (name, date, start, end) VALUES("Group-1", "2014-11-12", "19:00", "21:00");
INSERT INTO Groups (name, date, start, end) VALUES("Group-2", "2014-11-12", "19:00", "21:00");

SELECT id FROM Groups WHERE name = "Group-1";

UPDATE Clients SET group_id = (SELECT id FROM Groups WHERE name = "Group-1") WHERE name="Vsevolod";


