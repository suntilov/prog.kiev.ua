package ua.kiev.prog.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.kiev.prog.Service.ContactService;
import ua.kiev.prog.Entity.Contact;

import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping("/")
public class MyController {
    static final int DEFAULT_GROUP_ID = -1;

    private static String UPLOAD_LOCATION = "/home/zeppelin/tmp/";

    @Autowired
    private ContactService contactService;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("contacts", contactService.listContacts());
        return "index";
    }

    @RequestMapping("/contact_add_page")
    public String contactAddPage(Model model) {
        return "contact_add_page";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(@RequestParam String pattern, Model model) {
        //model.addAttribute("contacts", contactService.searchContacts(pattern));
        return "index";
    }

    @RequestMapping(value = "/contact/delete", method = RequestMethod.POST)
    public ResponseEntity<Void> delete(@RequestParam(value = "toDelete[]", required = false) long[] toDelete, Model model) {
        if (toDelete != null)
            contactService.deleteContact(toDelete);

        model.addAttribute("contacts", contactService.listContacts());

        return new ResponseEntity<Void>(HttpStatus.OK);  // Возврат 200 в скрипт на странице
    }

    @RequestMapping(value="/contact/add", method = RequestMethod.POST)
    public String contactAdd(@RequestParam String name,
                             @RequestParam String age,
                             Model model) throws IOException {
        Contact contact = new Contact(name, age);
        contactService.addContact(contact);

        model.addAttribute("contacts", contactService.listContacts());
        return "redirect:/"; // !!! затирает историю - для защиты от повторного добавления при перерисовке страницы или возврате
    }
}
