package ua.kiev.prog.Repository;

import ua.kiev.prog.Entity.Contact;

import java.util.List;

public interface ContactDAO {
    void add(Contact contact);
    void delete(Contact contact);
    void delete(long[] ids);
    List<Contact> list();
}
