package ua.kiev.prog.Repository;

import org.springframework.stereotype.Repository;
import ua.kiev.prog.Entity.Contact;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository  // не нужно описывать been
public class ContactDAOImpl implements ContactDAO {

    @PersistenceContext  // работает как @Autowired
    private EntityManager entityManager;

    @Override
    public void add(Contact contact) {
        entityManager.merge(contact);
    }

    @Override
    public void delete(Contact contact) {
        entityManager.remove(contact);
    }

    @Override
    public void delete(long[] ids) {
        Contact c;
        for (long id : ids) {
            c = entityManager.getReference(Contact.class, id);
            entityManager.remove(c);
        }
    }

    @Override
    public List<Contact> list() {
        Query query;
        String statement;
            statement = "SELECT c FROM Contact c";
            query = entityManager.createQuery(statement, Contact.class);
        return (List<Contact>) query.getResultList();
    }


}