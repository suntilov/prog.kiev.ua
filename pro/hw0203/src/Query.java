import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="query")
public class Query {

    @XmlAttribute(name="count")
    private int count;
    @XmlAttribute(name="yahoo:created")
    private String created;
    @XmlAttribute
    private String lang;
    @XmlElement
    private Results results;

    public Query() {
    }


    @Override
    public String toString() {
        return "Query{" +
                "count=" + count +
                ", created='" + created + '\'' +
                ", lang='" + lang + '\'' +
                ", results=" + results +
                '}';
    }
}
