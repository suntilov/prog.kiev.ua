package ua.kiev.prog.user;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class UserList {
    private static final UserList userList = new UserList();
    private static final int LIMIT = 100;

    private final Gson gson;
    private final List<User> list = new ArrayList<User>();

    public static UserList getInstance() {
        return userList;
    }

    private UserList() {
        gson = new GsonBuilder().create();
    }

    public synchronized void add(User user) {
        if (isUserExists(user))
            return;
        if (list.size() == LIMIT)
            list.remove(0);
        list.add(user);
    }

    public boolean isUserExists(User user) {
        for(User u : list) {
            if (u.getLogin().compareTo(user.getLogin()) == 0)
                return true;
        }
        return false;
    }

    public synchronized String toJSON() {
        return gson.toJson(new JsonUsers(list));
    }
}
