package ua.kiev.prog.user;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class User {
    private String login;
    private String privateTo;

    public User(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPrivateTo() {
        return privateTo;
    }

    public void setPrivateTo(String privateTo) {
        this.privateTo = privateTo;
    }

    public String toJSON() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

    public static User fromJSON(String s) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(s, User.class);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", privateTo='" + privateTo + '\'' +
                '}';
    }
}
