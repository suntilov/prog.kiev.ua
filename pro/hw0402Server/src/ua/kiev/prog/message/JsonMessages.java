package ua.kiev.prog.message;

import java.util.ArrayList;
import java.util.List;

public class JsonMessages {
    private final List<Message> list;

    public JsonMessages(List<Message> sourceList, String login, int fromIndex) {
        this.list = new ArrayList<>();
        for (int i = fromIndex; i < sourceList.size(); i++) {
            Message message = sourceList.get(i);
            if (message.getTo() == null || message.getTo().isEmpty() || message.getTo().compareTo(login) == 0)
                list.add(message);
        }
    }
}
