package ua.kiev.prog.servlet;

import ua.kiev.prog.user.User;
import ua.kiev.prog.user.UserList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static ua.kiev.prog.utils.Utils.requestBodyToArray;

public class AuthorizeServlet extends HttpServlet {
    private UserList userList = UserList.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        byte[] buf = requestBodyToArray(req);
        String bufStr = new String(buf, StandardCharsets.UTF_8);

        User user = User.fromJSON(bufStr);
        if (user != null)
            userList.add(user);
        else
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
}
