public class Container {
    @Save
    private String stringField;
    @Save
    private int intField;

    public Container() {
    }

    public Container(String stringField, int intField) {
        this.stringField = stringField;
        this.intField = intField;
    }

    public String getStringField() {
        return stringField;
    }

    public void setStringField(String stringField) {
        this.stringField = stringField;
    }

    public int getIntField() {
        return intField;
    }

    public void setIntField(int intField) {
        this.intField = intField;
    }

    @Override
    public String toString() {
        return "Container{" +
                "stringField='" + stringField + '\'' +
                ", intField=" + intField +
                '}';
    }
}
