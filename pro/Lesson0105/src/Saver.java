import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Saver {

    private Object obj;
    private String fileName;

    public Saver() {
    }

    public Object getObj() {
        return obj;
    }

    public Saver setObj(Object obj) {
        this.obj = obj;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public Saver setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public void store() {
        try (PrintWriter file = new PrintWriter(fileName)) {
            Field[] fields = obj.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Save.class)) {
                    if (Modifier.isPrivate(field.getModifiers()))
                        field.setAccessible(true);
                    file.println(field.get(obj));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void restore() {
        try (BufferedReader file = new BufferedReader(new FileReader(fileName))) {
            Field[] fields = obj.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Save.class)) {
                    String value = file.readLine();
                    // TODO replace to ClassForName
                    if (Modifier.isPrivate(field.getModifiers()))
                        field.setAccessible(true);
                    if (field.getType() == int.class)
                        field.set(obj, Integer.parseInt(value));
                    else if (field.getType() == String.class)
                        field.set(obj, value);
                    else if (field.getType() == long.class)
                        field.set(obj, Long.parseLong(value));
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
