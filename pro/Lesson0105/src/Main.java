/**
 3. Написать код, который сериализирует и десериализирует в/из файла все значения полей класса, которые
 отмечены аннотацией @Save.
 */
public class Main {

    public static void main(String[] argv) {

        Container source = new Container("test", 1);

        Saver saver = new Saver()
                .setObj(source)
                .setFileName("c:\\tmp\\container.txt");
        saver.store();

        Container destination = new Container();
        saver.setObj(destination).restore();

        System.out.println(source);
        System.out.println(destination);
    }
}
