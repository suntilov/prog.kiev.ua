/**
 * Created by user on 07.09.2016.
 */
public class Main {
    public static void main(String[] args) {


        Line line1 = new Line(new Point(1, 1), new Point(2, 2));
        Line line2 = line1.clone();

        line1.getA().setX(3);

        System.out.println(line1);
        System.out.println(line2);

    }
}
