/**
 * Created by user on 07.09.2016.
 */
public class Line implements Cloneable {

    private Point a;
    private Point b;

    public Line(Point a, Point b) {
        this.a = a;
        this.b = b;
    }

    public Line() {
    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) {
        this.a = a;
    }

    public Point getB() {
        return b;
    }

    public void setB(Point b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "Line{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }

    @Override
    protected Line clone() {
        try {

            Line line = (Line)super.clone();
            line.setA(new Point(this.getA().getX(), this.getA().getY()));
            line.setB(new Point(this.getB().getX(), this.getB().getY()));
            return line;
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
