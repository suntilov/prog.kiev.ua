/**
 * Created by user on 05.09.2016.
 */
public class main {

    Action ac = new Action();

    Thread thOne = new Thread(new SingleThread("Push", ac, true));
    Thread thTwo = new Thread(new SingleThread("Pull", ac, false));

    thOne.start();
    thTwo.start();
}
