import java.io.File;

/**
 * Created by user on 22.08.2016.
 */
public class SaveToXML implements Saver {

    File file;

    public SaveToXML() {
    }

    public SaveToXML(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public void save(Human human) {

    }

    @Override
    public Human load() {
        return null;
    }
}
