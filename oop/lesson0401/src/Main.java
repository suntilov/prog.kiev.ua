/**
 * Created by user on 22.08.2016.
 */

import java.io.File;

public class Main {
    public static void main(String[] args) {

        Human human = new Human("11111", "222222222", 48);

        human.save(new SaveToCSV(new File("human.csv")));

        Saver saver = new SaveToCSV(new File("human.csv"));
        Human restoredHuman = saver.load();

        System.out.println(human);
        System.out.println(restoredHuman);
    }
}
