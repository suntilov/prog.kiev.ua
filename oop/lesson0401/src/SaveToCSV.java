/**
 * Created by user on 22.08.2016.
 */
import java.io.*;

public class SaveToCSV implements Saver {

    private File file;

    public SaveToCSV() {
        super();
    }

    public SaveToCSV(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public void save(Human human) {
        try (PrintWriter pw = new PrintWriter(this.file)){
            pw.println(human.getName() + ";" + human.getLastName() + ";" + human.getAge());
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }

    @Override
    public Human load() {
        try (BufferedReader br = new BufferedReader(new FileReader(this.file))) {
            String humanLine = br.readLine();
            String[] humanProperties = humanLine.split(";");
            String name = humanProperties[0];
            String lastName = humanProperties[1];
            int age = Integer.valueOf(humanProperties[2]);
            return new Human(name, lastName, age);
        }
        catch (IOException e) {
            System.out.println(e);
        }
        return null;
    }
}
