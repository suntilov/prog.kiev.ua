/**
 * Created by user on 22.08.2016.
 */
public interface Saver {

    void save(Human human);
    Human load();
}
