/**
 * Created by user on 07.09.2016.
 */
public class Student {

    private String name;
    private String lastName;
    private int age;
    private int average;

    public Student() {
    }

    public Student(String name, String lastName, int age, int average) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.average = average;
    }

    public String getName() {
        return name;
    }

    public Student setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Student setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Student setAge(int age) {
        this.age = age;
        return this;
    }

    public int getAverage() {
        return average;
    }

    public Student setAverage(int average) {
        this.average = average;
        return this;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", average=" + average +
                '}';
    }
}
