import java.io.File;
import java.lang.reflect.Field;

/**
 * Created by user on 07.09.2016.
 */
public class Main {

    public static void main(String[] args) {

        Student s1 = new Student();

        s1.setName("11")
                .setAge(11)
                .setAverage(5)
                .setLastName("222");

        if (s1.getClass() == Student.class) {
            System.out.println("it is student");
        }

        File a = new File("a");

        if (!Student.class.equals(a.getClass()))
            System.out.println("it is not student");

        Class<?> class1 = Student.class;
        Class<?> class2 = a.getClass();

        if (class1 != class2)
            System.out.println("It is not student");

        Field[] fileArray = class1.getDeclaredFields();

        for (Field fields : fileArray) {
            System.out.println(fields);
        }

        try {
            Field field = class1.getDeclaredField("name");
            field.setAccessible(true);
            field.set(s1, "3333");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        System.out.println(s1);
    }
}
