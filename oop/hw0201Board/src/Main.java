import cf.bamba.board.Board;
import cf.bamba.board.Shape;
import cf.bamba.board.ShapeFactory;

public class Main {

    public static void main(String argv[]) {

        ShapeFactory factory = new ShapeFactory();

        Shape rectangle = factory.createRectangle(1,10,2,20);
        Shape circle    = factory.createCircle(3,3,4,4);
        Shape triangle  = factory.createTriangle(1,1,10,10,15,1);

        Board board = new Board();

        board.moveShape(rectangle, 1);
        board.moveShape(rectangle, 2);
        board.moveShape(circle, 3);
        board.moveShape(triangle, 4);

        board.removeShape(rectangle);
        board.moveShape(rectangle, 2);

        System.out.println(board);
    }
}
