package cf.bamba.board;

import cf.bamba.board.Circle;
import cf.bamba.board.Point;
import cf.bamba.board.Rectangle;

public class ShapeFactory {

    public ShapeFactory() {
    }

    public Rectangle createRectangle(double ax, double ay, double bx, double by) {
        Point a = new Point(ax, ay);
        Point b = new Point(bx, by);
        return new Rectangle(a, b);
    }

    public Circle createCircle(double ax, double ay, double bx, double by) {
        Point a = new Point(ax, ay);
        Point b = new Point(bx, by);
        return new Circle(a, b);
    }

    public Triangle createTriangle(double ax, double ay, double bx, double by, double cx, double cy) {
        Point a = new Point(ax, ay);
        Point b = new Point(bx, by);
        Point c = new Point(cx, cy);
        return new Triangle(a, b,  c);
    }

 }
