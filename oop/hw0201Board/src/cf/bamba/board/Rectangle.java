package cf.bamba.board;

import cf.bamba.board.Point;

public class Rectangle extends Shape {

    public Point a;
    public Point b;

    public Rectangle() {
    }

    public Rectangle(Point a, Point b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return Point.length(a, b) * 2 + Point.width(a, b) * 2;
    }

    @Override
    public double getArea() {
        return Point.length(a, b) * Point.width(a, b);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "a=" + a +
                ", b=" + b +
                ", perimeter=" + getPerimeter() +
                ", area=" + getArea() +
                '}';
    }
}
