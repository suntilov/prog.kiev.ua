package cf.bamba.board;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

public class Point {

    public double x;
    public double y;

    public Point() {
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static double length(Point a, Point b) {
        return abs(a.x - b.x);
    }

    public static double width(Point a, Point b) {
        return abs(a.y - b.y);
    }

    public static double diagonal(Point a, Point b) {
        double length = length(a, b);
        double width = width(a, b);
        return sqrt(length * length + width * width);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
