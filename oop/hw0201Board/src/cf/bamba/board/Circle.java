package cf.bamba.board;

public class Circle extends Shape {

    public Point a;
    public Point b;

    public Circle() {
    }

    public Circle(Point a, Point b) {
        this.a = a;
        this.b = b;
    }

    public double radius() {
        return Point.diagonal(a, b);
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius();
    }

    @Override
    public double getArea() {
        double radius = radius();
        return Math.PI * (radius * radius);
    }


    @Override
    public String toString() {
        return "Circle{" +
                "a=" + a +
                ", b=" + b +
                ", perimeter=" + getPerimeter() +
                ", area=" + getArea() +
                '}';
    }
}
