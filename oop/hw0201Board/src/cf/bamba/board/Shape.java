package cf.bamba.board;

public abstract class Shape {
    public abstract double getPerimeter();
    public abstract double getArea();
}
