package cf.bamba.board;

public class Board {

    final public static int size = 4;

    private Shape[] cell = new Shape[size];

    public Board() {
    }

    public void moveShape(Shape shape, int cellNumber) {
        if (isValid(cellNumber) && cell[cellNumber - 1] == null) {
            removeShape(shape);
            cell[cellNumber - 1] = shape;
        }
    }

    public void removeShape(Shape shape) {
        for (int i = 0; i < size; i++) {
            if (cell[i] == shape) {
                cell[i] = null;
                return;
            }
        }
    }

    public double summaryShapesArea() {
        double area = 0;
        for (int i = 0; i < size; i++) {
            if (cell[i] != null)
                area += cell[i].getArea();
        }
        return area;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < size; i++) {
            sb.append(i + 1 + ") ");
            sb.append(cell[i] == null ? "free" : cell[i]);
            sb.append(System.lineSeparator());
        }

        sb.append("Summary area: ");
        sb.append(summaryShapesArea());
        sb.append(System.lineSeparator());

        return sb.toString();
    }

    private boolean isValid(int cellNumber) {
        if (cellNumber > 0 && cellNumber <= size)
            return true;
        return false;
    }
}
