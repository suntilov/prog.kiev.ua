package cf.bamba.board;

import cf.bamba.board.Point;
import cf.bamba.board.Shape;

import static java.lang.Math.sqrt;

public class Triangle extends Shape {

    public Point a;
    public Point b;
    public Point c;

    public Triangle() {
    }

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double getPerimeter() {
        return Point.diagonal(a, b) + Point.diagonal(b, c) + Point.diagonal(c, a);
    }

    @Override
    public double getArea() {
        double A = Point.diagonal(a, b);
        double B = Point.diagonal(b, c);
        double C = Point.diagonal(c, a);
        double p = (A + B + C) / 2;
        return sqrt(p * (p-A) * (p-B) * (p-C));
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", perimeter=" + getPerimeter() +
                ", area=" + getArea() +
                '}';
    }
}
