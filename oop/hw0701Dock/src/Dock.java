import java.util.ArrayList;
import java.util.List;

public class Dock {

    private String name;
    private List<Box> boxes = new ArrayList<>();


    public Dock(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void loadBox(Box box) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boxes.add(box);
        System.out.println("Dock " +
                "name='" + name + '\'' +
                ", box " + box +
                " loaded.");
    }

    public synchronized void unloadShip(Ship ship) {
        while (!ship.isEmpty())
            loadBox(ship.unloadBox());
    }

    @Override
    public String toString() {
        return "Dock{" +
                "name='" + name + '\'' +
                ", boxes=" + boxes.size() +
                '}';
    }
}
