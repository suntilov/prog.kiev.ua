import java.util.ArrayList;
import java.util.List;

public class Ship {
    private String name;
    private List<Box> boxes;

    public Ship(String name) {
        this.name = name;
        this.boxes = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ship loadBox(Box box) {
        boxes.add(box);
        return this;
    }

    public Box unloadBox() {
        Box box = boxes.get(0);
        if (box != null) {
            boxes.remove(box);
            System.out.println("Ship " +
                    "name='" + name + '\'' +
                    ", box " + box +
                    " unloaded.");

        }
        return box;
    }

    public boolean isEmpty() {
        return boxes.isEmpty();
    }

    public int nBoxes() {
        return boxes.size();
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", boxes=" + boxes.size() +
                '}';
    }
}
