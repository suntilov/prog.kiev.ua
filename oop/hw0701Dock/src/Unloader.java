public class Unloader implements Runnable {

    private Dock dock;
    private Ship ship;

    public Unloader(Dock dock, Ship ship) {
        super();
        this.dock = dock;
        this.ship = ship;
    }

    public Thread unloadShip() {
        Thread thread = new Thread(this);
        thread.start();
        return thread;
    }

    @Override
    public void run() {
        dock.unloadShip(ship);
    }
}
