/**
 Существуют три корабля. На каждом из них 10 ящиков груза.
 Они одновременно прибыли в порт в котором только два
 дока. Скорость разгрузки 1 ящик в 0.5 сек. Напишите
 программу которая управляя кораблями позволит им
 правильно разгрузить груз.
 */

public class Main {
    public static void main(String[] args) {

        Ship ship1 = new Ship("Silver Dream");
        Ship ship2 = new Ship("Northern Star");
        Ship ship3 = new Ship("Martha Ann");

        ship1.loadBox(new Box("Banana")).loadBox(new Box("Coconut")).loadBox(new Box("Orange"))
                .loadBox(new Box("Mango")).loadBox(new Box("Papaya")).loadBox(new Box("Lemon"))
                .loadBox(new Box("Lime")).loadBox(new Box("Apple")).loadBox(new Box("Pineapple"))
                .loadBox(new Box("Joke"));

        ship2.loadBox(new Box("Banana")).loadBox(new Box("Coconut")).loadBox(new Box("Orange"))
                .loadBox(new Box("Mango")).loadBox(new Box("Papaya")).loadBox(new Box("Lemon"))
                .loadBox(new Box("Lime")).loadBox(new Box("Apple")).loadBox(new Box("Pineapple"))
                .loadBox(new Box("Coke"));

        ship3.loadBox(new Box("Banana")).loadBox(new Box("Coconut")).loadBox(new Box("Orange"))
                .loadBox(new Box("Mango")).loadBox(new Box("Papaya")).loadBox(new Box("Lemon"))
                .loadBox(new Box("Lime")).loadBox(new Box("Apple")).loadBox(new Box("Pineapple"))
                .loadBox(new Box("Yoyo"));

        Dock dockA = new Dock("A");

        Dock dockB = new Dock("B");

        Port port = new Port("Ningbo-Shoushan");
        port.addDock(dockA).addDock(dockB);
        port.arrive(ship1).arrive(ship2).arrive(ship3);

        System.out.println("Before unloading ships:");
        System.out.println(port);

        port.unloadShips();

        System.out.println("After unloading ships:");
        System.out.println(port);

        port.departure(ship1.getName()).departure(ship2.getName()).departure(ship3.getName());

        System.out.println("Port after departure ships:");
        System.out.println(port);
    }
}
