public class Box {
    private String name;

    public Box(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Box{" +
                "name='" + name + '\'' +
                '}';
    }
}
