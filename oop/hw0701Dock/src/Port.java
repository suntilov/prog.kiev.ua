import java.util.ArrayList;
import java.util.List;

public class Port {
    private String name;
    private List<Dock> docks;
    private List<Ship> ships;
    private int nextDockIndex = -1;

    public Port(String name) {
        this.name = name;
        this.docks = new ArrayList<>();
        this.ships = new ArrayList<>();
    }

    public Port() {
    }

    public Port addDock(Dock dock) {
        docks.add(dock);
        return this;
    }

    public void removeDock(String name) {
        Dock dock = findDock(name);
        if (dock != null)
            docks.remove(dock);
    }

    public Dock findDock(String name) {
        return docks.stream().filter(x -> x.getName().equals(name)).findFirst().get();
    }

    public Port arrive(Ship ship) {
        ships.add(ship);
        return this;
    }

    public Port departure(String name) {
        Ship ship = findShip(name);
        if (ship != null)
            ships.remove(ship);
        return this;
    }

    public Ship findShip(String name) {
        return ships.stream().filter(x -> x.getName().equals(name)).findFirst().get();
    }

    public void unloadShips() {
        List<Thread> threads = new ArrayList<>();
        for (Ship ship: ships) {
            if (!ship.isEmpty()) {
                Unloader unloader = new Unloader(nextDock(), ship);
                threads.add(unloader.unloadShip());
            }
        }
        joinThreads(threads);
    }

    @Override
    public String toString() {
        return "Port{" +
                "name='" + name + '\'' +
                ", docks=" + docks +
                ", ships=" + ships +
                '}';
    }

    private Dock nextDock() {
        nextDockIndex++;
        if (nextDockIndex >= docks.size())
            nextDockIndex = 0;
        return docks.get(nextDockIndex);
    }

    private void joinThreads(List<Thread> threads) {
        for (Thread thread : threads)
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}
