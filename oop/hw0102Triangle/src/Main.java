import cf.bamba.triangle.Triangle;

public class Main {

    public static void main(String argv[]) {

        Triangle triangle1 = new Triangle(10, 10, 10);
        System.out.println(triangle1);

        Triangle triangle2 = new Triangle(triangle1);
        triangle2.setB(22);
        System.out.println(triangle2);

        Triangle triangle3 = new Triangle(triangle2);
        triangle3.setC(22);
        System.out.println(triangle3);
    }
}
