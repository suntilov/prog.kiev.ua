import java.util.Arrays;
import java.util.Random;

/**
 * Created by user on 17.08.2016.
 */
public class Main {
    public static void main(String[] args) {
        int[] a = generateArray();
        int[] b = generateArray();
        int[] c = generateArray();

        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
        System.out.println(Arrays.toString(c));

        System.out.println(getSum(a, b, c));
    }

    public static int[] generateArray() {
        Random rn = new Random();
        int[] array = new int[5 + rn.nextInt(11)];
        for (int i = 0; i < array.length; i++) {
            array[i] = rn.nextInt(10);
        }
        return array;
    }

    public static int getSum(int[] a, int[] b, int[] c) {
        int result = 0;

        int len = a.length;
        if (b.length > len)
                len = b.length;
        if (c.length > len)
                len = c.length;


        for (int i = 0; i <len; i++) {
            try {
                result += a[i];
            } catch (ArrayIndexOutOfBoundsException e) {
            }
            try {
                result += b[i];
            } catch (ArrayIndexOutOfBoundsException e) {
            }
            try {
                result += c[i];
            } catch (ArrayIndexOutOfBoundsException e) {
            }
        }


        return result;
    }
}
