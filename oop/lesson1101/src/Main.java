import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by user on 19.09.2016.
 */
public class Main {
    public static void main(String[] args) {

        InetAddress address1 = null;
        InetAddress address2 = null;
        InetAddress address3 = null;
        try {
            address1 = InetAddress.getByName("www.google.com");
            address2 = InetAddress.getByName("www.fresh-cream.in.ua");
            address3 = InetAddress.getByName("www.intelserv.com");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        System.out.println(address1);
        System.out.println(address2);
        System.out.println(address3);
    }
}
