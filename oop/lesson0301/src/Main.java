/**
 * Created by user on 17.08.2016.
 */
public class Main {
    public static void main(String[] args) {

        A classA = new B();
        classA.plus(5);
        System.out.println(classA.a);
    }
}

class A {
    int a = 3;
    public void plus(int b) {
        a += b;
    }
}

class B extends A {
    int a = 5;

    @Override
    public void plus(int b) {
        a *= b;
    }
}