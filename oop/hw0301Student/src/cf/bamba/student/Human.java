package cf.bamba.student;

import java.time.LocalDate;
import java.util.Date;

public class Human {

    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private Sex sex;

    public Human() {
    }

    public Human(String firstName, String lastName, LocalDate birthday, Sex sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.sex = sex;
    }

    public Human(Human human) {
        this.firstName = human.firstName;
        this.lastName = human.lastName;
        this.birthday = human.birthday;
        this.sex = human.sex;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getFullName() {
        return lastName + " " + firstName;
    }

    public int getAge(LocalDate date) {
        int year1 = date.getYear();
        int year2 = birthday.getYear();
        return year1 - year2;
    }

    @Override
    public String toString() {
        return "Human{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", sex=" + sex +
                '}';
    }
}
