package cf.bamba.student;

public enum Sex {
    UNDEFINED, MALE, FEMALE
}
