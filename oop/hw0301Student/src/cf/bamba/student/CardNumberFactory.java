package cf.bamba.student;

public class CardNumberFactory {

    private static int nextNumber = 100000;

    public CardNumberFactory() {
    }

    public String createCardNumber() {
        nextNumber++;
        return Integer.toString(nextNumber);
    }
}
