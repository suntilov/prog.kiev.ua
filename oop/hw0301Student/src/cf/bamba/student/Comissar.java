package cf.bamba.student;

import java.time.LocalDate;
import java.util.Arrays;

public class Comissar implements RecrutiersList {

    private String name;
    private LocalDate dateRecruit;
    private RecrutiersList recrutiersList;

    public Comissar() {
    }

    public Comissar(String name, LocalDate dateRecruit, RecrutiersList recrutiersList) {
        this.name = name;
        this.dateRecruit = dateRecruit;
        this.recrutiersList = recrutiersList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateRecruit() {
        return dateRecruit;
    }

    public void setDateRecruit(LocalDate dateRecruit) {
        this.dateRecruit = dateRecruit;
    }

    public RecrutiersList getRecrutiersList() {
        return recrutiersList;
    }

    public void setRecrutiersList(RecrutiersList recrutiersList) {
        this.recrutiersList = recrutiersList;
    }

    @Override
    public Human[] getRecruitList(LocalDate date) {
        return recrutiersList.getRecruitList(dateRecruit);
    }

    public void printRecruiters() {
        System.out.println("Comissar " + name);
        System.out.println("Recruiters:");
        Human[] recruiters = recrutiersList.getRecruitList(dateRecruit);
        for (int i = 0; i < recruiters.length; i++) {
            System.out.println(recruiters[i] + " age: " + recruiters[i].getAge(dateRecruit));
        }
    }

    @Override
    public String toString() {
        return "Comissar{" +
                "name='" + name + '\'' +
                ", dateRecruit=" + dateRecruit +
                ", recrutiersList=" + recrutiersList +
                '}';
    }
}
