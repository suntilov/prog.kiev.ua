package cf.bamba.student;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateBuilder {

    private DateBuilder() {
    }

    public static LocalDate getDate(String ddmmyyyy) {
        LocalDate result = null;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            result = LocalDate.parse(ddmmyyyy, formatter);
        }
        catch (DateTimeException e) {
        }
        return result;
    }
}
