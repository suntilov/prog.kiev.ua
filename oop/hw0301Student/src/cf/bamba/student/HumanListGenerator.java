package cf.bamba.student;

import java.time.LocalDate;
import java.util.ArrayList;

public class HumanListGenerator {

    private HumanListGenerator() {
    }

    public static ArrayList<Human> generate() {
        ArrayList<Human> humansList = new ArrayList<>();
        humansList.add(new Human("Ivan", "Ivanov", LocalDate.of(1980, 1,  1), Sex.MALE  ));
        humansList.add(new Human("Ivan", "Petrov", LocalDate.of(1980, 1,  2), Sex.MALE  ));
        humansList.add(new Human("Ivan", "Sidoro", LocalDate.of(1980, 1,  3), Sex.MALE  ));
        humansList.add(new Human("Anna", "Sidoro", LocalDate.of(1980, 1,  4), Sex.FEMALE));
        humansList.add(new Human("Kote", "Tatkov", LocalDate.of(1980, 1,  6), Sex.MALE  ));
        humansList.add(new Human("Lida", "Mamkin", LocalDate.of(1980, 1,  7), Sex.FEMALE));
        humansList.add(new Human("Lisa", "Babkin", LocalDate.of(1980, 1,  8), Sex.FEMALE));
        humansList.add(new Human("Kola", "Dedkin", LocalDate.of(1980, 1,  9), Sex.MALE  ));
        humansList.add(new Human("Pete", "Vnukin", LocalDate.of(1980, 1, 10), Sex.MALE  ));
        humansList.add(new Human("Gena", "Chukin", LocalDate.of(1981, 1, 10), Sex.MALE  ));
        humansList.add(new Human("Boba", "Bookin", LocalDate.of(1982,11, 11), Sex.MALE  ));
        return humansList;
    }
}
