package cf.bamba.student;

import java.time.LocalDate;

public interface RecrutiersList {

    Human[] getRecruitList(LocalDate date);
}
