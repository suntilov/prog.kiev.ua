package cf.bamba.student;

import java.time.LocalDate;
import java.util.*;

import static cf.bamba.student.Sex.*;

public class Group implements RecrutiersList {

    private String groupName;
    private Student[] studentArray = new Student[10];

    CardNumberFactory cardNumberFactory;

    private Student findStudent;
    public int foundIndex;

    public Group() {
    }

    public Group(String groupName, CardNumberFactory cardNumberFactory) {
        this.groupName = groupName;
        this.cardNumberFactory = cardNumberFactory;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public CardNumberFactory getCardNumberFactory() {
        return cardNumberFactory;
    }

    public void setCardNumberFactory(CardNumberFactory cardNumberFactory) {
        this.cardNumberFactory = cardNumberFactory;
    }

    public void add(Student student) throws ArrayIsFullException {
        for (int i = 0; i < studentArray.length; i++) {
            if (studentArray[i] == null) {
                studentArray[i] = student;
                studentArray[i].setCardNumber(cardNumberFactory.createCardNumber());
            }
            if (studentArray[i] == student) /// TODO: replace to human's comparator
                return;
        }
        throw new ArrayIsFullException();
    }

    public void add(ArrayList<Human> humanList) {
        humanList.forEach((human) -> {
            try {
                add(new Student(human));
            }
            catch (ArrayIsFullException e) {
                e.printStackTrace();
            }
        });
    }

    public void remove(Student student) {
        for (int i = 0; i < studentArray.length; i++)
            if (studentArray[i] == student) {
                studentArray[i] = null;
                return;
            }
    }

    public Student find(Student student) {
        this.findStudent = student;
        foundIndex = -1;
        return findNext();
    }

    public Student findNext() {
        for (int i = foundIndex + 1; i < studentArray.length; i++) {
            if (studentArray[i] == null)
                continue;
            if (compare(findStudent, studentArray[i]) == 0)
                return studentArray[i];
        }
        return null;
    }

    public Student findByCardNumber(String cardNumber) {
        for (int i = 0; i < studentArray.length; i++) {
            if (studentArray[i] == null)
                continue;
            if (studentArray[i].getCardNumber().compareToIgnoreCase(cardNumber) == 0)
                return studentArray[i];
        }
        return null;
    }

    public void setStudentProgress(String cardNumber, int progress) {
        try {
            Student student = findByCardNumber(cardNumber);
            student.setProgress(progress);
        }
        catch (NullPointerException e) {
        }
    }

    public int compare(Student o1, Student o2) {
        if (o1 == null)
            return -1;
        if (o2 == null)
            return 1;

        int result = 0;

        if (o1.getCardNumber() != null && o2.getCardNumber() != null) {
            result = o1.getCardNumber().compareToIgnoreCase(o2.getCardNumber());
            if (result != 0)
                return result;
        }

        if (o1.getLastName() != null && o2.getLastName() != null) {
            result = o1.getLastName().compareToIgnoreCase(o2.getLastName());
            if (result != 0)
                return result;
        }

        if (o1.getFirstName() != null && o2.getFirstName() != null) {
            result = o1.getFirstName().compareToIgnoreCase(o2.getFirstName());
            if (result != 0)
                return result;
        }

        if (o1.getBirthday() != null && o2.getBirthday() != null) {
            result = o1.getBirthday().isAfter(o2.getBirthday()) ? 1 :
                    o1.getBirthday().isBefore(o2.getBirthday()) ? -1 : 0;
            if (result != 0)
                return result;
        }

        if (o1.getSex() != null && o2.getSex() != null) {
            if (o1.getSex() != o2.getSex())
                return 1;
        }

        result = o1.getProgress() > o2.getProgress() ? 1 :
                o1.getProgress() < o2.getProgress() ? -1 : 0;
        if (result != 0)
            return result;

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Group: " + groupName);
        sb.append(System.lineSeparator());
        for (int i = 0; i < studentArray.length; i++) {
            sb.append(i + 1 + ") ");
            if (studentArray[i] != null)
                sb.append(studentArray[i]);
            sb.append(System.lineSeparator());
        }
        return sb.toString();
    }

    public void sort(StudentFields studentFields) {
        Arrays.sort(studentArray, GroupSortFactory.getComparator(studentFields));
        System.out.println("Group sorted by " + studentFields.toString() + ".");
    }

    /// Student[] -> Human[]
    @Override
    public Human[] getRecruitList(LocalDate date) {
        /// TODO: replace to .map().filter()
        ArrayList<Human> humanList = new ArrayList<>();
        for (int i = 0; i < studentArray.length; i++) {
            if (studentArray[i] != null &&
                studentArray[i].getSex() != null &&
                    studentArray[i].getSex() == MALE &&
                    studentArray[i].getAge(date) >= 18)
                humanList.add(studentArray[i]);
        }
        Human[] humanArray = new Human[humanList.size()];
        return humanList.toArray(humanArray);
    }
}
