package cf.bamba.student;

import java.util.Comparator;

public class GroupSortFactory {

    private GroupSortFactory() {
    }

    public static Comparator<Student> getComparator(StudentFields studentFields) {

        switch (studentFields) {

            case NAME:
                return (a, b) -> a == null ? 1 : b == null ? -1 :
                        a.getFirstName().compareToIgnoreCase(b.getFirstName());

            case BIRTHDAY:
                return (a, b) -> a == null ? 1 : b == null ? -1 :
                        a.getBirthday() == null ? 1 :b.getBirthday() == null ? -1 :
                                a.getBirthday().isAfter(b.getBirthday()) ? 1 :
                                        a.getBirthday().isBefore(b.getBirthday()) ? -1 : 0;

            case CARDNUMBER:
                return (a, b) -> a == null ? 1 : b == null ? -1 :
                        a.getCardNumber().compareToIgnoreCase(b.getCardNumber());

            case PROGRESS:
                return (a, b) -> a == null ? 1 : b == null ? -1 :
                        a.getProgress() > b.getProgress() ? 1 :
                                a.getProgress() < b.getProgress() ? -1 : 0;

            case LASTNAME:
            default:
                return (a, b) -> a == null ? 1 : b == null ? -1 :
                        a.getLastName().compareToIgnoreCase(b.getLastName());

        }
    }
}
