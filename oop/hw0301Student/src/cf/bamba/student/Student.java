package cf.bamba.student;

import java.time.LocalDate;

public class Student extends Human {

    private String cardNumber;
    private int progress;


    public Student() {
        super();
    }

    public Student(Human human) {
        super(human);
    }

    public Student(String firstName, String lastName, LocalDate birthday, Sex sex) {
        super(firstName, lastName, birthday, sex);
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    @Override
    public String toString() {
        return "Student{" +
                super.toString() +
                ", cardNumber=" + cardNumber +
                ", progress=" + progress +
                "}";
    }

}
