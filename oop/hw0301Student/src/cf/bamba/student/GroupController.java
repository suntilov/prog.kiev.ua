package cf.bamba.student;

import java.util.Scanner;

import static cf.bamba.student.Sex.MALE;

public class GroupController {

    private Group group;

    public GroupController(Group group) {
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public boolean addSudent() {
        Human human = inputHumanData();
        try {
            Student student = new Student(human);
            group.add(student);
            return true;
        }
        catch (ArrayIsFullException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Human inputHumanData() {

        Human human = new Human();
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter new student's data or leave blank for exit, than press Enter.");

        System.out.println("First name: ");
        human.setFirstName(scan.next());

        System.out.println("Last name: ");
        human.setLastName(scan.next());

        System.out.println("Birthday (dd/mm/yyyy): ");
        human.setBirthday(DateBuilder.getDate(scan.next()));

        System.out.println("Sex (m/f): ");
        String sex = scan.next();
        human.setSex(sex.compareToIgnoreCase("m") == 0 ? MALE :
                sex.compareToIgnoreCase("f") == 0 ? Sex.FEMALE : Sex.UNDEFINED);

        return human;
    }
}
