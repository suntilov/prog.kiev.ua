package cf.bamba.student;

public enum StudentFields {
    NAME,
    LASTNAME,
    BIRTHDAY,
    CARDNUMBER,
    PROGRESS
}
