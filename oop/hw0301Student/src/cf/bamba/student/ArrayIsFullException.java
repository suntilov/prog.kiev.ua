package cf.bamba.student;

public class ArrayIsFullException extends Exception {

    @Override
    public String getMessage() {
        return "Insert Fault. Array is full.";
    }
}
