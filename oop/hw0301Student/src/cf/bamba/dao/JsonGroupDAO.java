package cf.bamba.dao;

import com.google.gson.Gson;

import cf.bamba.student.Group;
import com.google.gson.GsonBuilder;

import java.io.*;

public class JsonGroupDAO implements GroupDAO {

    public JsonGroupDAO() {
    }

    @Override
    public void storeGroup(Group group) {
        try(Writer writer = new OutputStreamWriter(new FileOutputStream(group.getGroupName() + fileExtention), "UTF-8")) {
            Gson gson = new GsonBuilder().create();
            writer.write(gson.toJson(group, Group.class));
        }
        catch (IOException e) {
            System.out.println(e.getStackTrace());
        }
    }

    @Override
    public Group restoreGroup(String name) {
        try(Reader reader = new InputStreamReader(new FileInputStream(name + fileExtention), "UTF-8")){
            Gson gson = new GsonBuilder().create();
            Group group = gson.fromJson(reader, Group.class);
            return group;
        }
        catch (IOException e) {
            System.out.println(e.getStackTrace());
        }
        return null;
    }

    private final String fileExtention = ".json";
}
