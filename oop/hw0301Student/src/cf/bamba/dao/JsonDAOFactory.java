package cf.bamba.dao;

public class JsonDAOFactory extends DAOFactory {

    public JsonDAOFactory() {
    }

    @Override
    public GroupDAO getGroupDAO() {
        return new JsonGroupDAO();
    }
}
