package cf.bamba.dao;


import cf.bamba.student.Group;

public interface GroupDAO {
    void storeGroup(Group group);
    Group restoreGroup(String name);
}
