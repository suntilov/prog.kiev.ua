package cf.bamba.dao;

public abstract class DAOFactory {

    public static final int JSON = 1;

    public abstract GroupDAO getGroupDAO();

    public static DAOFactory getDAOFactory(int whichFactory) throws IllegalArgumentException {
        switch (whichFactory) {
            case JSON:
                return new JsonDAOFactory();
        }
        throw new IllegalArgumentException("Wrong factory.");
    }

}
