import java.time.LocalDate;
import java.util.ArrayList;

import cf.bamba.dao.DAOFactory;
import cf.bamba.dao.GroupDAO;
import cf.bamba.student.*;

import static cf.bamba.dao.DAOFactory.JSON;
import static cf.bamba.student.StudentFields.*;

public class Main {

    public static void main(String[] args) {

        /// Create and fill group
        Group group = new Group("Group Java OOP", new CardNumberFactory());
        group.add(HumanListGenerator.generate());

        /// Find student and remove from group
        Student findCriterion = new Student();
        findCriterion.setFirstName("Ivan");
        Student toRemove = group.find(findCriterion);
        group.remove(toRemove);

        /// Find next student with some criterion and remove from group
        toRemove = group.findNext();
        group.remove(toRemove);



        /// Interactive add student
//        GroupController groupController = new GroupController(group);
//        groupController.addSudent();



        /// Set progress for students
        group.setStudentProgress("100003", 5);
        group.setStudentProgress("100004", 4);
        group.setStudentProgress("100005", 4);
        group.setStudentProgress("100006", 4);
        group.setStudentProgress("100007", 5);

        /// Sort by progress
        group.sort(PROGRESS);
        System.out.println(group);

        /// Sort by last name
        group.sort(LASTNAME);
        System.out.println(group);



        /// Create recruiter[HUMAN] from group[STUDENT] and print
        Comissar comissar = new Comissar("Voenkom", LocalDate.now(), group);
        comissar.printRecruiters();


        DAOFactory daoFactory = DAOFactory.getDAOFactory(JSON);
        GroupDAO groupDAO = daoFactory.getGroupDAO();

        /// Store group
        groupDAO.storeGroup(group);

        /// Restore group
        group = groupDAO.restoreGroup(group.getGroupName());

        /// Print restored group
        System.out.println();
        System.out.println(group);
    }
}
