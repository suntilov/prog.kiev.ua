import java.math.BigInteger;

public class Factorial implements Runnable {

    private int n;

    public Factorial(int n) {
        this.n = n;
    }

    @Override
    public void run() {
        BigInteger factorial = new BigInteger("1");
        for (int i = 1; i < n; i++) {
            factorial = factorial.multiply(new BigInteger("" + i));
        }
        System.out.println(factorial);
    }
}
