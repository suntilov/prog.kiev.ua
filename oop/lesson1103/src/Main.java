import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by user on 19.09.2016.
 */
public class Main {
    public static void main(String[] args) {

        Map<String, Integer> map = new HashMap<String, Integer>();

        readUrl(map, "site_list.txt");
        checkUrl(map);
        map.forEach((String s, Integer i) -> System.out.println(s + " return " + i));
    }

    public static void readUrl(Map<String, Integer> map, String source) {
        try (BufferedReader f =new BufferedReader(new FileReader(source))) {
            String str="";
            for (;(str = f.readLine()) != null;) {
                map.put(str, 0);
            }
        }

        catch (IOException e) {
            //e.printStackTrace();
        }
    }

    public static void checkUrl(Map<String, Integer> map) {
        Set<String> sites = map.keySet();
        for (String site: sites) {
            try {
                URL url = new URL(site);
                HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                map.put(site, httpCon.getResponseCode());
            }
            catch (MalformedURLException e) {
                //e.printStackTrace();
            }
            catch (IOException e) {
                //e.printStackTrace();
            }
        }
    }
}
