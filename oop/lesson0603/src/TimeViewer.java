import java.text.SimpleDateFormat;

public class TimeViewer implements Runnable {

    @Override
    public void run() {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        System.out.println(sdf.format(System.currentTimeMillis()));
    }
}
