import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {

        //startTimeViewer();

        ScheduledExecutorService exs = Executors.newSingleThreadScheduledExecutor();
        ScheduledFuture<?> res = exs.scheduleAtFixedRate(new TimeViewer(), 10, 1, TimeUnit.SECONDS);

        System.out.println("END");

    }

    public static void startTimeViewer() {
        ScheduledExecutorService exs = Executors.newSingleThreadScheduledExecutor();
        ScheduledFuture<?> res = exs.scheduleAtFixedRate(new TimeViewer(), 10, 3, TimeUnit.SECONDS);
    }
}
