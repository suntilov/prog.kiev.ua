import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadShellSort {

    private int[] arrayInt;
    private int N;

    public ThreadShellSort(int[] arrayInt) {
        this.arrayInt = arrayInt;
        N = arrayInt.length;
    }

    public void sort() {
        int nThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        for (int k = N / 2; k > 0; k /= 2)
            executor.execute(new SortInterval(k));
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
    }



    private class SortInterval implements Runnable {

        private int k;

        public SortInterval(int k) {
            super();
            this.k = k;
        }

        @Override
        public void run() {
            for (int i = k; i < N; i += 1) {
                int t = arrayInt[i];
                int j;
                for (j = i; j >= k; j -= k) {
                    if (t < arrayInt[j - k])
                        arrayInt[j] = arrayInt[j - k];
                    else
                        break;
                }
                arrayInt[j] = t;
            }
        }
    }
}
