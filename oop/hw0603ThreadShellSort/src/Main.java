/**
 * Напишите многопоточный вариант сортировки массива методом Шелла.
 */

import java.util.Random;

public class Main {



    public static void main(String[] args) {

        int[] arrayOne = newArrayInt(10000000);
        int[] arrayTwo = arrayOne.clone();

        ShellSort shellSort = new ShellSort(arrayOne);
        long time = System.currentTimeMillis();
        shellSort.sort();
        time = System.currentTimeMillis() - time;
        System.out.println("One thread: " + time);
        //for (int i: arrayOne) System.out.println(i);

        System.out.println("");

        ThreadShellSort threadShellSort = new ThreadShellSort(arrayTwo);
        time = System.currentTimeMillis();
        threadShellSort.sort();
        time = System.currentTimeMillis() - time;
        System.out.println("Multi thread: " + time);
        //for (int i: arrayTwo) System.out.println(i);
    }



    private static int[] newArrayInt(int size) {
        int[] arrayInt = new int[size];
        Random random = new Random();
        for (int i = 0; i < arrayInt.length; i++) {
            arrayInt[i] = random.nextInt(size);
        }
        return arrayInt;
    }
}
