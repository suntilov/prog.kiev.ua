public class ShellSort {

    int[] arrayInt;

    public ShellSort(int[] arrayInt) {
        this.arrayInt = arrayInt;
    }

    public void sort() {
        int t;
        int N = arrayInt.length;
        for(int k = N/2; k > 0; k /= 2) {
            for (int i = k; i < N; i += 1) {
                t = arrayInt[i];
                int j;
                for (j = i; j >= k; j -= k) {
                    if (t < arrayInt[j - k])
                        arrayInt[j] = arrayInt[j - k];
                    else
                        break;
                }
                arrayInt[j] = t;
            }
        }
    }
}
