import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by user on 29.08.2016.
 */
public class FileOperation {

    private FileOperation() {
    }

    public static void copyFile(File from, File to) throws IOException {

        if (from == null || to == null) {
            throw new IllegalArgumentException("Null pointer");
        }
        try (FileInputStream fis = new FileInputStream(from);
             FileOutputStream fos = new FileOutputStream(to)) {

            byte[] buffer = new byte[1024 * 1024];
            int byteCopy = 0;
            for (;(byteCopy = fis.read(buffer)) > 0; ) {
                fos.write(buffer, 0, byteCopy);
            }

        } catch (IOException e) {
            throw e;
        }
    }
}
