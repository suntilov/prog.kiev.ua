import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by user on 29.08.2016.
 */
public class Main {
    public static void main(String[] args) {

        File fileOne = new File("a.txt");

        boolean isFileCreated = false;
        try {
            isFileCreated = fileOne.createNewFile();
        }
        catch (IOException e) {
            System.out.println(e);
        }
        System.out.println(isFileCreated);

        File fileTwo = new File("b.txt");

        fileOne.renameTo(fileTwo);

        fileTwo.delete();

        File folderTwo = new File("FOLDER");
        folderTwo.mkdirs();

        File fileThree = new File(folderTwo, "c.txt");
        try {
            fileThree.createNewFile();
        } catch (IOException e) {
            System.out.println(e);
        }

        fileThree.delete();
        folderTwo.delete();



        File folderOne = new File(".");
        File[] files = folderOne.listFiles();
        for (File file : files) {
            System.out.println(file);
        }

        File from = new File("D:/usv/prog.kiev.ua/oop/Методические материалы/Java OOP (All cource).pdf");
        File to = new File("test.pdf");
        try {
            FileOperation.copyFile(from, to);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
