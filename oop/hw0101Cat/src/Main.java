import java.time.LocalDate;

import cf.bamba.cat.Cat;

public class Main {
    public static void main(String argv[]) {

        Cat cat1 = new Cat();
        cat1.setBirthday(LocalDate.of(2010, 1, 11));
        System.out.println(cat1);

        cat1.feed();
        System.out.println(cat1);

        Cat father = new Cat();
        father.setBirthday(LocalDate.of(2000, 11, 12));
        Cat mother = new Cat();

        cat1.setFather(father);
        cat1.setMother(mother);
        cat1.familyTree();
    }
}
