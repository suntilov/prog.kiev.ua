package cf.bamba.cat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class Cat {

    private String          name;
    private String          breed;
    private String          color;
    private LocalDate       birthday;
    private String          sex;

    private Cat             father;
    private Cat             mother;

    private int             feedingHoursInterval;
    private LocalDateTime   nextFeed;

    public Cat() {
        birthday = LocalDate.now();
        feedingHoursInterval = 4;
        nextFeed = LocalDateTime.now();
    }

    public Cat(String name, String breed, String color, LocalDate birthday, String sex, Cat father, Cat mother,
               int feedingHoursInterval, LocalDateTime nextFeed) {
        this.name = name;
        this.breed = breed;
        this.color = color;
        this.birthday = birthday;
        this.sex = sex;
        this.father = father;
        this.mother = mother;
        this.feedingHoursInterval = feedingHoursInterval;
        this.nextFeed = nextFeed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Cat getFather() {
        return father;
    }

    public void setFather(Cat father) {
        this.father = father;
    }

    public Cat getMother() {
        return mother;
    }

    public void setMother(Cat mother) {
        this.mother = mother;
    }

    public int getFeedingHoursInterval() {
        return feedingHoursInterval;
    }

    public void setFeedingHoursInterval(int feedingHoursInterval) {
        this.feedingHoursInterval = feedingHoursInterval;
    }

    public LocalDateTime getNextFeed() {
        return nextFeed;
    }

    public void setNextFeed(LocalDateTime nextFeed) {
        this.nextFeed = nextFeed;
    }

    public int getAgeYears() {
        LocalDate now = LocalDate.now();
        Period period = Period.between(birthday, now);
        return period.getYears();
    }

    public int getAgeMonths() {
        LocalDate now = LocalDate.now();
        Period period = Period.between(birthday, now);
        return period.getMonths() % 12;
    }

    public Boolean isHungry() {
        if (nextFeed.isBefore(LocalDateTime.now()))
            return true;
        return false;
    }

    public void feed() {
        nextFeed = LocalDateTime.now().plusHours(feedingHoursInterval);
    }

     public void familyTree() {
         System.out.println("Sorry, family tree is under construction.");
     }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", breed='" + breed + '\'' +
                ", color='" + color + '\'' +
                ", birthday=" + birthday +
                ", sex='" + sex + '\'' +
                ", father=" + father +
                ", mother=" + mother +
                ", feedingHoursInterval=" + feedingHoursInterval +
                ", nextFeed=" + nextFeed +
                ", age=" + getAgeYears() + "years " + getAgeMonths() + "months" +
                ", is hungry=" + isHungry() +
                '}';
    }
}
