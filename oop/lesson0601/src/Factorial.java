import java.math.BigInteger;

/**
 * Created by user on 31.08.2016.
 */
public class Factorial implements Runnable {

    private int n;

    public Factorial() {
    }

    public Factorial(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    private BigInteger factorialCalculate() {
        BigInteger fact = new BigInteger("1");
        for (int i = 1; i < n; i++)
            fact = fact.multiply(new BigInteger("" + i));
        return fact;
    }

    @Override
    public void run() {
        Thread t = Thread.currentThread();
        System.out.println(t.getName() + "->" + n + "!=" + factorialCalculate());
    }
}
