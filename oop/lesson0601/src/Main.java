/**
 * Created by user on 31.08.2016.
 */
public class Main {

    public static void main(String[] args) {

        Thread thOne = new Thread(new Factorial(200));
        Thread thTwo = new Thread(new Factorial(100));

        thOne.start();
        thTwo.start();

        Thread[] threads = new Thread[200];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(new Factorial(i));
            threads[i].start();
            //threads[i].join(); - так делать нельзя, делай ещё один цикл
        }

        System.out.println(Thread.currentThread().getName());
    }
}
