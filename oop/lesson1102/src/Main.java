import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        try {

            URL url = new URL("https://prog.kiev.ua/forum/index.php/topic,2379.165.html");
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            Map<String, List<String>> hm = httpCon.getHeaderFields();
            hm.forEach((n, key) -> System.out.println(n + " - " + key));
            StringBuilder sb = new StringBuilder();

            try(BufferedReader br = new BufferedReader(new InputStreamReader(httpCon.getInputStream(), "utf-8"))) {
                String text;
                for(; (text = br.readLine()) != null;) {
                    sb.append(text).append(System.lineSeparator());
                }
            }

            String text = sb.toString();
            System.out.println(text);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
