import cf.bamba.vector3d.Vector3d;

public class Main {

    public static void main(String argv[]) {

        Vector3d a = new Vector3d(5, 6, 7);
        Vector3d b = new Vector3d(2, 3, 4);

        Vector3d c = a.add(b);
        System.out.println(c);

        c = a.sub(b);
        System.out.println(c);

        c = a.vectorMul(b);
        System.out.println(c);

        double f = a.scalarMul(b);
        System.out.println(a.toString() + '*' + b + '=' + f);
    }
}
