package cf.bamba.vector3d;

public class Vector3d {

    private double x;
    private double y;
    private double z;

    public Vector3d() {
    }

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3d(Vector3d vector3d) {
        this.x = vector3d.x;
        this.y = vector3d.y;
        this.z = vector3d.z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public Vector3d add(Vector3d b) {
        Vector3d result = new Vector3d();
        result.setX(x + b.getX());
        result.setY(y + b.getY());
        result.setZ(z + b.getZ());
        return result;
    }

    public Vector3d sub(Vector3d b) {
        Vector3d result = new Vector3d();
        result.setX(x - b.getX());
        result.setY(y - b.getY());
        result.setZ(z - b.getZ());
        return result;
    }

    public Vector3d vectorMul(Vector3d b) {
        Vector3d result = new Vector3d();
        result.setX(x * b.getX());
        result.setY(y * b.getY());
        result.setZ(z * b.getZ());
        return result;
    }

    public double scalarMul(Vector3d b) {
        return x * b.getX() + y * b.getY() + z * b.getZ();
    }

    @Override
    public String toString() {
        return "Vector3d{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
