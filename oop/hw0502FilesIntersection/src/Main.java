import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        try(PrintWriter a = new PrintWriter("file1.txt")){
            a.println("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
            a.println("Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,");
            a.println("when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
            a.println("It has survived not only five centuries, but also the leap into electronic typesetting,");
            a.println("remaining essentially unchanged.");
        }
        catch(FileNotFoundException e){
            System.out.println("ERROR FILE WRITE");
        }

        try(PrintWriter a = new PrintWriter("file2.txt")){
            a.println("It is a long established fact that a reader will be distracted by the readable content of a");
            a.println("page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less");
            a.println("normal distribution of letters, as opposed to using 'Content here, content here', making it");
            a.println("look like readable English.");
        }
        catch(FileNotFoundException e){
            System.out.println("ERROR FILE WRITE");
        }

        Set<String> wordSet1 = file2set("file1.txt");
        Set<String> wordSet2 = file2set("file2.txt");

        wordSet1.retainAll(wordSet2);

        set2file(wordSet1, "file3.txt");
    }



    public static Set<String> file2set(String fileName) {
        Set<String> wordSet = new HashSet<>();
        try(BufferedReader f = new BufferedReader(new FileReader(fileName))){
            String str;
            for(; (str=f.readLine()) != null; ) {
                str.replace(".,:!", "");
                String[] strValues = str.split(" ");
                wordSet.addAll(Arrays.asList(strValues));
            }
        }
        catch(IOException e){
            System.out.println("ERROR FILE READ");
        }
        return wordSet;
    }



    public static void set2file(Set<String> set, String fileName) {
        try(PrintWriter a = new PrintWriter(fileName)){
            boolean isFirst = true;
            for (String word : set) {
                a.print(isFirst ? word : " " + word);
                isFirst = false;
            }
        }
        catch(FileNotFoundException e){
            System.out.println("ERROR FILE WRITE");
        }
    }
}

