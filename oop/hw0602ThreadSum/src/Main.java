/**
 Написать код для многопоточного подсчета суммы элементов
 массива целых чисел. Сравнить скорость подсчета с простым
 алгоритмом.
*/

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        int[] arrayInt = newArrayInt(99999999);

        long time = System.currentTimeMillis();
        int sum = 0;
        for (int i: arrayInt) sum += i;
        time = System.currentTimeMillis() - time;
        System.out.println("One thread method: " + sum + ", time (ms):" + time);

        int nThreads = Runtime.getRuntime().availableProcessors();
        time = System.currentTimeMillis();
        sum = manyThreadsSum(arrayInt, nThreads);
        time = System.currentTimeMillis() - time;
        System.out.println("Many threads method: " + sum + ", time (ms):" + time);
    }



    public static int manyThreadsSum(int[] arrayInt, int nThreads) {
        int size = arrayInt.length / nThreads;
        ThreadSum[] threads = new ThreadSum[nThreads];

        for (int i = 0; i < nThreads; i++) {
            threads[i] = new ThreadSum(arrayInt,
                                       i * size,
                                       i + 1 == nThreads ?
                                               arrayInt.length - 1 :
                                               (i + 1) * size - 1);
            threads[i].start(); ///< first start all threads
        }
        try {
            for (ThreadSum thread: threads)
                thread.join();  ///< then join all threads
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        int sum = 0;
        for (ThreadSum threadSum: threads) {
            sum += threadSum.getSum();
        }
        return sum;
    }



    public static int[] newArrayInt(int size) {
        int[] arrayInt = new int[size];
        Random random = new Random();
        for (int i = 0; i < arrayInt.length; i++)
            arrayInt[i] = random.nextInt(10);
        return arrayInt;
    }
}
