import jdk.internal.instrumentation.InstrumentationMethod;

/**
 * Created by user on 07.09.2016.
 */
public class ThreadSum extends Thread {

    private int[] arrayInt;
    private int beg;
    private int end;
    private int sum;

    public ThreadSum(int[] arrayInt, int beg, int end) {
        super();
        this.arrayInt = arrayInt;
        this.beg = beg;
        this.end = end;
        this.sum = 0;
    }

    public int getSum() {
        return sum;
    }

    @Override
    public void run() {
        for (int i = beg; i <= end; i++) {
            sum += arrayInt[i];
        }
    }
}
