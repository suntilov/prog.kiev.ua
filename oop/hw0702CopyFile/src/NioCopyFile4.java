import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


public class NioCopyFile4 implements Runnable {

    private int bufferSize;
    private String source;
    private String destination;
    private long startPosition;
    private long nBuffers;


    public NioCopyFile4(int bufferSize, String source, String destination, long startPosition, long nBuffers) {
        super();
        this.bufferSize = bufferSize;
        this.source = source;
        this.destination = destination;
        this.startPosition = startPosition;
        this.nBuffers = nBuffers;
    }

    @Override
    public void run() {

        try (FileChannel sourceChannel = FileChannel.open(Paths.get(source), StandardOpenOption.READ);
             FileChannel destinationChannel = FileChannel.open(Paths.get(destination),
                     StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {

            ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
            long position = startPosition;

            long byteRead = 0;

            for (long counter = 0; counter < nBuffers; counter++) {
                if((byteRead = sourceChannel.read(buffer, position)) < 0)
                    break;
                buffer.flip();
                destinationChannel.write(buffer, position);
                position += byteRead;
                buffer.clear();
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public static long fileSize(Path path) {
        long size = 0;
        try {
            size = Files.size(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return size;
    }

    public static boolean createIfNotExists(Path path) {
        if (!Files.exists(path))
            try {
                Files.createFile(path);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        return false;
    }
}
