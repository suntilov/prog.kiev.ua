import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ArrayBlockingQueue;

public class NioCopyFile2 {

    private int bufferSize;

    public NioCopyFile2(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public void copy(String source, String destination) {

        Path sourcePath = Paths.get(source);
        Path destinationPath = Paths.get(destination);
        createIfNotExists(destinationPath);

        long size = fileSize(sourcePath);


        try (FileChannel sourceChannel =
                     FileChannel.open(sourcePath, StandardOpenOption.READ);
             AsynchronousFileChannel destinationChannel =
                     AsynchronousFileChannel.open(destinationPath,
                             StandardOpenOption.WRITE)
        ) {

            ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
            long position = 0;
            long progress = 0;

            System.out.println("");
            ArrayBlockingQueue<Boolean> done = new ArrayBlockingQueue<Boolean>(1);
            long byteRead = 0;
            while ((byteRead = sourceChannel.read(buffer, position)) >= 0) {
                buffer.flip();
                destinationChannel.write(buffer, position, done, new CompletionHandler<Integer, ArrayBlockingQueue<Boolean>>() {
                    @Override
                    public void completed(Integer result, ArrayBlockingQueue<Boolean> attachment) {
                        try {
                            attachment.put(true);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failed(Throwable exc, ArrayBlockingQueue<Boolean> attachment) {
                    }
                });

                done.take();
                position += byteRead;
                buffer.clear();

                progress = showProgress(size, position, progress);
            }
            System.out.println("");
        } catch (IOException | InterruptedException e) {
            System.out.println(e);
        }
    }

    private long fileSize(Path path) {
        long size = 0;
        try {
            size = Files.size(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return size;
    }

    private long showProgress(long size, long wrote, long prevProgress) {
        long newProgress = 100 * wrote / size;
        long delta = newProgress - prevProgress;
        for (long i = 0; i < delta; i++)
            System.out.print("*");
        return newProgress;
    }

    private boolean createIfNotExists(Path path) {
        if (!Files.exists(path))
            try {
                Files.createFile(path);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
    }
}
