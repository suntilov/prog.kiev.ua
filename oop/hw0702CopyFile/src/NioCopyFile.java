import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class NioCopyFile {

    private int bufferSize;

    public NioCopyFile(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public void copy(String source, String destination) {

        Path destinationPath = Paths.get(destination);

        try (AsynchronousFileChannel sourceAfc =
                     AsynchronousFileChannel.open(Paths.get(source), StandardOpenOption.READ);
             AsynchronousFileChannel destinationAfc =
                     AsynchronousFileChannel.open(destinationPath, StandardOpenOption.TRUNCATE_EXISTING,
                             StandardOpenOption.CREATE, StandardOpenOption.WRITE)
        ) {

            ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
            long position = 0;

            while (true) {
                buffer.clear();
                Future<Integer> readResult = sourceAfc.read(buffer, position);
                Integer byteRead = readResult.get();

                if (byteRead < 0)
                    break;
                buffer.flip();

                Future<Integer> writeResult = destinationAfc.write(buffer, position);

                position += byteRead;

                while(!writeResult.isDone());
            }

        } catch (IOException | InterruptedException | ExecutionException e) {
            System.out.println(e);
        }
    }
}
