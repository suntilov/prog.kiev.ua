import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ArrayBlockingQueue;

public class NioCopyFile3 implements Runnable {

    private int bufferSize;
    private String source;
    private String destination;
    private long startPosition;
    private long nBuffers;


    public NioCopyFile3(int bufferSize, String source, String destination, long startPosition, long nBuffers) {
        super();
        this.bufferSize = bufferSize;
        this.source = source;
        this.destination = destination;
        this.startPosition = startPosition;
        this.nBuffers = nBuffers;
    }

    @Override
    public void run() {

        Path sourcePath = Paths.get(source);
        Path destinationPath = Paths.get(destination);

        try (FileChannel sourceChannel =
                     FileChannel.open(sourcePath, StandardOpenOption.READ);
             AsynchronousFileChannel destinationChannel =
                     AsynchronousFileChannel.open(destinationPath,
                             StandardOpenOption.WRITE)
        ) {

            ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
            long position = startPosition;

            ArrayBlockingQueue<Boolean> done = new ArrayBlockingQueue<Boolean>(1);
            long byteRead = 0;

            for (long counter = 0; counter < nBuffers; counter++) {

                 if((byteRead = sourceChannel.read(buffer, position)) < 0)
                     break;

                buffer.flip();
                destinationChannel.write(buffer, position, done,
                        new CompletionHandler<Integer, ArrayBlockingQueue<Boolean>>() {
                    @Override
                    public void completed(Integer result, ArrayBlockingQueue<Boolean> attachment) {
                        try {
                            attachment.put(true);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failed(Throwable exc, ArrayBlockingQueue<Boolean> attachment) {
                    }
                });

                done.take();
                position += byteRead;
                buffer.clear();
            }
        } catch (IOException | InterruptedException e) {
            System.out.println(e);
        }
    }

    public static long fileSize(Path path) {
        long size = 0;
        try {
            size = Files.size(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return size;
    }

    public static boolean createIfNotExists(Path path) {
        if (!Files.exists(path))
            try {
                Files.createFile(path);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        return false;
    }
}
