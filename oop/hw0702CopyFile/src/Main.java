/**
 Реализуйте программу многопоточного копирования файла
 блоками, с выводом прогресса на экран.

 Исходный файл 1.5 Гб

 Result:
 Test 1 (IO copy) time: 2748
 Test 2 (NIO copy) time: 5986
 Test 3 (NIO copy) time: 4607
 Test 4 (NIO copy) time: 3879
 Test 5 (NIO copy) time: 3948

 Многопоточные методы копирования файла работают медленнее однопоточного.
 Операции в оперативной памяти выполняются быстрее, чем запись на диск, даже в однопоточном режиме.
 Физическую запись на диск на потоки не разобъёшь.
 Увеличение скорости копирования файла можно получить за счёт увеличения буфера -
 т.е за счёт уменьшения количества обращений к диску,
 но не за счёт увеличения количества потоков.
 */

import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Main {

    final public static int defaultBufferSize = 1024 * 4;

    public static void main(String[] args) {

        String home = System.getProperty("user.home");
        String fileSeparator = System.getProperty("file.separator");
        String source = home + fileSeparator + "temp" + fileSeparator + "test.iso";
        String destination = home + fileSeparator + "temp" + fileSeparator + "test2.iso";


        destination = home + fileSeparator + "temp" + fileSeparator + "test1.iso";
        Long time = System.currentTimeMillis();
        CopyFile copyFile = new CopyFile(defaultBufferSize);
        copyFile.copy(source, destination);
        System.out.println("Test 1 (IO copy) time: " + (System.currentTimeMillis() - time));


        destination = home + fileSeparator + "temp" + fileSeparator + "test2.iso";
        time = System.currentTimeMillis();
        NioCopyFile nioCopyFile = new NioCopyFile(defaultBufferSize);
        nioCopyFile.copy(source, destination);
        System.out.println("Test 2 (NIO copy) time: " + (System.currentTimeMillis() - time));


        destination = home + fileSeparator + "temp" + fileSeparator + "test3.iso";
        time = System.currentTimeMillis();
        NioCopyFile2 nioCopyFile2 = new NioCopyFile2(defaultBufferSize);
        nioCopyFile2.copy(source, destination);
        System.out.println("Test 3 (NIO copy) time: " + (System.currentTimeMillis() - time));


        int nThreads = Runtime.getRuntime().availableProcessors() * 2;
        long fileSize =  NioCopyFile3.fileSize(Paths.get(source));
        long sectionSize = fileSize / (long)nThreads;
        long nSections = (fileSize / sectionSize) + ((fileSize % sectionSize) == 0 ? 0 : 1);
        long nBuffers = (sectionSize / (long)defaultBufferSize) + ((sectionSize / (long)defaultBufferSize) == 0 ? 0 : 1);


        {
            destination = home + fileSeparator + "temp" + fileSeparator + "test4.iso";
            time = System.currentTimeMillis();
            NioCopyFile3.createIfNotExists(Paths.get(destination));
            ExecutorService executor = Executors.newFixedThreadPool((int) nSections);
            for (int i = 0; i < nSections; i++) {
                NioCopyFile3 thread = new NioCopyFile3(defaultBufferSize, source, destination,
                        i * sectionSize, nBuffers);
                executor.execute(thread);
            }
            executor.shutdown();
            while (!executor.isTerminated()) {
            }
            System.out.println("Test 4 (NIO copy) time: " + (System.currentTimeMillis() - time));
        }


        {
            destination = home + fileSeparator + "temp" + fileSeparator + "test5.iso";
            NioCopyFile4.createIfNotExists(Paths.get(destination));
            time = System.currentTimeMillis();
            Thread[] threads = new Thread[(int)nSections];
            for (int i = 0; i < nSections; i++) {
                threads[i] = new Thread(new NioCopyFile4(defaultBufferSize, source, destination,
                        i * sectionSize, nBuffers));
                threads[i].start();
            }
            for (int i = 0; i < nSections; i++) {
                try {
                    threads[i].join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Test 5 (NIO copy) time: " + (System.currentTimeMillis() - time));
        }
    }
}
