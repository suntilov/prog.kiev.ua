import java.io.*;

public class CopyFile {

    private int bufferSize;

    public CopyFile(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public void copy(String source, String destination) {

        try (FileInputStream fis = new FileInputStream(new File(source));
             FileOutputStream fos = new FileOutputStream(new File(destination))) {

            byte[] buffer = new byte[bufferSize];
            int byteCopy = 0;
            for (; (byteCopy = fis.read(buffer)) > 0;) {
                fos.write(buffer, 0, byteCopy);
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
