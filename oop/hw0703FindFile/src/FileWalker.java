import java.io.File;
import java.util.List;

public class FileWalker extends Thread {

    private FolderList folderList;
    private String fileName;
    private List<String> found;

    public FileWalker(String threadName, FolderList folderList, String fileName, List<String> found) {
        super("FileWalker");
        setName(threadName);
        this.folderList = folderList;
        this.fileName = fileName.replace("?", ".?").replace("*", ".*?");
        this.found = found;
        start();
    }

    @Override
    public void run() {
        while (!folderList.isFinished()) {
            String folderName = folderList.get();
            if (folderName != null) {
                File folder = new File(folderName);
                if (folder != null) {
                    File[] files = folder.listFiles();
                    if (files != null) {
                        for (File file : files) {
                            if (file.getName().matches(fileName))
                            synchronized (found) {
                                found.add(file.getPath());
                            }
                        }
                    }
                }
            }
        }
    }
}
