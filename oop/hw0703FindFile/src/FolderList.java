import java.util.LinkedList;

public class FolderList {

    private LinkedList<String> folderList = new LinkedList();
    private boolean finished = false;

    public synchronized void put(String path) {
            folderList.add(path);
            notify();
    }

    public synchronized String get() {
            while(!finished && folderList.isEmpty())
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            String folder = folderList.poll();
            notify();
            return folder;
    }

    public synchronized boolean isFinished() {
        if (finished && folderList.isEmpty())
            return true;
        return false;
    }

    public synchronized void setFinished(boolean finished) {
            this.finished = finished;
            notify();
    }
}
