import java.io.File;
import java.nio.file.Files;

public class FolderWalker extends Thread {

    private String startFolder;
    private FolderList folderList;

    public FolderWalker(String startFolder, FolderList folderList) {
        super("FolderWalker");
        this.startFolder = startFolder;
        this.folderList = folderList;
        start();
    }

    @Override
    public void run() {
        File root = new File(startFolder);
        walk(root);
        folderList.setFinished(true);
    }

    private void walk(File folder) {
        if (folder.isDirectory()) {
            folderList.put(folder.getPath());
            File[] files = folder.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory() && !Files.isSymbolicLink(file.toPath()))
                        walk(file);
                }
            }
        }
    }
}
