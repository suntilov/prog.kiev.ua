/**
 Реализуйте процесс многопоточного поиска файла в
 файловой системе. Т.е. вы вводите название файла и в какой
 части файловой системы его искать. Программа должна
 вывести на экран все адреса в файловой системе файлов с
 таким названием.
 */

import java.util.List;

public class Main {
    public static void main(String[] args) {

        String fileName = "*.pdf";
        String startFolder = System.getProperty("user.home");

        long time = System.currentTimeMillis();
        int nThreads = Runtime.getRuntime().availableProcessors() + 1;

        FileFind fileFind = new FileFind(fileName, startFolder, nThreads);
        List<String> found = fileFind.find();

        time = System.currentTimeMillis() - time;

        for (String file: found)
            System.out.println(file);
        System.out.println(time);
    }
}
