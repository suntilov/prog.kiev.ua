import java.util.ArrayList;
import java.util.List;

public class FileFind {

    private String fileName;
    private String startFolder;
    private int nThreads;
    private List<String> found = new ArrayList<>();

    public FileFind(String fileName, String startFolder, int nThreads) {
        this.fileName = fileName;
        this.startFolder = startFolder;
        this.nThreads = nThreads;
    }

    public List<String> find() {
        FolderList folderList = new FolderList();
        FolderWalker folderWalker = new FolderWalker(startFolder, folderList);
        Thread[] threads = new Thread[nThreads];
        threads[0] = folderWalker;
        for (int i = 1; i < nThreads; i++)
            threads[i] = new FileWalker("FileWalker" + i, folderList, fileName, found);
        for (int i = 0; i < nThreads; i++) {
            try {
                if (threads[i] != null)
                    threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return found;
    }
}
