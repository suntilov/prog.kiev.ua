package cf.bamba.phone;

public interface SimCard {
    Network getNetwork();
    String getPhoneNumber();
}
