package cf.bamba.phone;

public interface Conversation {
    public void connect(Phone phone);
    public void speak(Phone phone, String text);
    public void disconnect(Phone phone);
    public Boolean isValid();
}
