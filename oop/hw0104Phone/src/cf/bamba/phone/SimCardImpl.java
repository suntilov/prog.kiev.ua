package cf.bamba.phone;

public class SimCardImpl implements SimCard {
    private Network network;
    private String phoneNumber;

    public SimCardImpl(Network network, String phoneNumber) {
        this.network = network;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public Network getNetwork() {
        return network;
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }
}
