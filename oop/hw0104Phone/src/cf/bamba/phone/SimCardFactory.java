package cf.bamba.phone;

public class SimCardFactory {

    public SimCardFactory() {
    }

    public SimCard createSimCard(Network network, String phoneNumber) {
        // Здесь мы создаём объект Сим-карта и возвращаем на него ссылку
        return new SimCardImpl(network, phoneNumber);
        // Можно было-бы обойтись и без фабрики, просто написав в main.java:
        // Network network = new NetworkImpl();
        // SimCard simCard1 = new SimCardImpl(network, "111-111-11-11");
        // SimCard simCard2 = new SimCardImpl(network, "222-222-22-22");
        // но тогда мы бы жёстко жёстко связали бы нашу программу с конкретной реализацией
        // сим карт, а мы хотим в программе оперировать неким интерфейсом, а создание поручить
        // Фабрике, причём всегда сможем заменить Фабрику либо реализацию Фабрики либо реализацию Сим-карты
        // без внесения изменений в программу, 
        // где используетсяна интерфейс сим-карты, а не конкретная её реализация
    }
}
