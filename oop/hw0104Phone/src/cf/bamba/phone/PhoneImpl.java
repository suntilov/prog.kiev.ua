package cf.bamba.phone;

public class PhoneImpl implements Phone {

    // Это композиция. Когда в отдном объекте есть ссылка на другой, независимый объект
    // В классе Телефон объявлена ссылка на объект Сим-карта
    private SimCard simCard;

    private Conversation conversation;

    public PhoneImpl() {
    }

    public PhoneImpl(SimCard simCard) {
        // Это инициализация ссылки на объект Сим-карты в конструкторе
        this.simCard = simCard;
    }

    @Override
    public void setSimCard(SimCard simCard) {
        // Это инициализация ссылки на объект Сим-карты в специальном методе
        // Можно использовать любой способ - и через конструктор и через отдельный метод
        this.simCard = simCard;
    }

    @Override
    public void removeSimCard() {
        simCard = null;
    }

    @Override
    public String getNumber() {
        return simCard.getPhoneNumber();
    }

    public Conversation getConversation() {
        return conversation;
    }

    @Override
    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }


    @Override
    public void register() {
        simCard.getNetwork().register(this);
    }

    @Override
    public void unRegister() {
        // Так как в свойстве simCard класса Телефон хранится указатель на объект Сим-карты,
        // мы можем вызвать метод getNetwork(), который вернёт нам ссылку на объект Сеть,
        // в которой зарегистрирована Сим-карта, а у полученного объекта Сеть вызвать метод
        // анулирования регистрации
        simCard.getNetwork().unregister(this);
    }

    @Override
    public Boolean call(String phoneNumber) {
        // У нашего класса Телефон есть ссылка на разговор, это свойство conversation
        // После создания объекта Телефон, conversation == NULL, то есть объекта Разговор нет.
        // 1) Метод call создаёт объект Разговор.
        conversation = simCard.getNetwork().createConversation(this, phoneNumber);
        // 2) То же самое можен написать подробно:
        Network network = simCard.getNetwork();
        conversation = network.createConversation(this, phoneNumber)
        // Результатом Первого (краткого) исполнения и Второго (подробного) исполнения
        // будет объект Разговор, ссылку на который мы получаем в свойстве conversation
        
        // и возвращаем булевое значение: True - разговор создан, False - разговор создать не удалось,
        // например, потому что номер не действителен.
        return conversation.isValid();
    }

    @Override
    public void connectToConversation(String phoneNumber) {
        simCard.getNetwork().connectToConversation(conversation, phoneNumber);
    }

    @Override
    public void speak(String text) {
        System.out.println(this + " Speaking: " + text);
        conversation.speak(this, text);
    }

    @Override
    public void listen(String text) {
        System.out.println(this + " Hearing: " + text);
    }

    @Override
    public void endCall() {
        conversation.disconnect(this);
    }

    @Override
    public String toString() {
        return "Phone{" + simCard.getPhoneNumber() + '}';
    }
}
