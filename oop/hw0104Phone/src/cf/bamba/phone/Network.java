package cf.bamba.phone;

public interface Network {
    void register(Phone phone);
    void unregister(Phone phone);

    Conversation createConversation(Phone phone, String phoneNumber);
    Boolean connectToConversation(Conversation conversation, String phoneNumber);
}
