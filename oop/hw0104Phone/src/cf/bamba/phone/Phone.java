package cf.bamba.phone;

public interface Phone {
    void register();
    void unRegister();
    void setSimCard(SimCard simCard);
    void removeSimCard();
    String getNumber();
    void setConversation(Conversation conversation);
    Boolean call(String phoneNumber);
    void connectToConversation(String phoneNumber);
    void speak(String text);
    void listen(String text);
    void endCall();
}
