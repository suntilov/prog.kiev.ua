package cf.bamba.phone;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ConversationImpl implements Conversation {

    private Map<String, Phone> phoneMap = new HashMap<String, Phone>();

    public ConversationImpl() {
    }

    @Override
    public void connect(Phone phone) {
        phoneMap.put(phone.getNumber(), phone);
    }

    @Override
    public void speak(Phone phone, String text) {
        Collection<Phone> phoneList = phoneMap.values();
        for(Phone item : phoneList) {
            if (item != phone)
                item.listen(text);
        }
    }

    @Override
    public void disconnect(Phone phone) {
        phoneMap.remove(phone.getNumber());
        phone.setConversation(null);
        if (phoneMap.size() < 2)
            disconnectAll();
    }

    @Override
    public Boolean isValid() {
        if (phoneMap.size() > 1)
            return true;
        else
            return false;
    }

    private void disconnectAll() {
        Collection<Phone> phoneList = phoneMap.values();
        for(Phone item : phoneList) {
            item.endCall();
        }
    }
}
