package cf.bamba.phone;

import java.util.*;

public class NetworkImpl implements Network {
    private Map<String, Phone> phoneList = new HashMap<String, Phone>();

    public NetworkImpl() {
    }

    @Override
    public void register(Phone phone) {
        phoneList.put(phone.getNumber(), phone);
    }

    @Override
    public void unregister(Phone phone) {
        phoneList.remove(phone);
    }

    @Override
    public Conversation createConversation(Phone phone1, String phone2Number) {
        Conversation conversation = new ConversationImpl();
        Phone phone2 = findPhone(phone2Number);
        if (phone2 != null) {
            conversation.connect(phone1);
            conversation.connect(phone2);
            phone1.setConversation(conversation);
            phone2.setConversation(conversation);
        }
        else {
            phone1.listen("Wrong number.");
        }
        return conversation;
    }

    @Override
    public Boolean connectToConversation(Conversation conversation, String phoneNumber) {
        Phone phone = findPhone(phoneNumber);
        if (phone != null) {
            conversation.connect(phone);
            phone.setConversation(conversation);
            return true;
        }
        return false;
    }

    private Phone findPhone(String phoneNumber) {
        return phoneList.get(phoneNumber);
    }
}
