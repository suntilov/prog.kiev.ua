import cf.bamba.phone.*;

public class Main {
    public static void main(String argv[]) {

        // Создаём объект Сеть
        Network network = new NetworkImpl();

        // Создаём объект Фабрика сим-карточек, чтобы генерировать их сколько нам нужно
        // Паттерн "Фабрика"
        SimCardFactory simCardFactory = new SimCardFactory();

        // Создаём объекты сим-карточек при помощи фабрики, 
        // каждая сим-карточка принадлежит Сети и имеет Номер
        SimCard simCard1 = simCardFactory.createSimCard(network, "111-111-11-11");
        SimCard simCard2 = simCardFactory.createSimCard(network, "222-222-22-22");
        SimCard simCard3 = simCardFactory.createSimCard(network, "333-333-33-33");

        // Создаём объекты телефонов
        
        // В первом случае вставляем сим-карту в телефон при помощи конструктора
        Phone phone1 = new PhoneImpl(simCard1);
        // Во втором создание объекта выполняется без вставки сим-карты
        Phone phone2 = new PhoneImpl();
        // а сим-карта вставляется предназначенным для этого методом
        phone2.setSimCard(simCard2);
        // Третий телефон создаём для того, чтобы далее показать реализацию чата, а не простого телефонного разговора
        Phone phone3 = new PhoneImpl(simCard3);  ///< For chat example

        // Регистрируем телефоны в телефонной сети, см. реализацию метода register()
        // Пока телефоны не зарегистрированы в сети, они не могут звонить друг-другу - нет связующего звена
        phone1.register();
        phone2.register();
        phone3.register();

        // После регистрации звоним с Телефона 1
        if (phone1.call("222-222-22-22")) {
            // Если дозвонились с Первого телефона на Второй, подсоединяем Третий телефон к разговору - Чат
            phone1.connectToConversation("333-333-33-33"); ///< Third speaker

            // Далее разговор, который через объект Сеть передаётся всем остальным телефонам подключённым к чату
            phone1.speak("Hello!");
            phone2.speak("Hi!");
            phone3.speak("How are you?");
            phone1.speak("Fine. And You?");
            phone2.speak("I'm fine too. Good bye.");
            phone1.speak("Good bye.");

            // Первый телефон прерывает соединение
            phone1.endCall();
            

            phone2.speak("Good bye.");
            phone3.speak("Good bye.");

            // Второй телефон прерывает соединение    
            phone2.endCall();
        }

        phone1.call("444-444-44-44"); ///< Wrong phone number
    }
}
