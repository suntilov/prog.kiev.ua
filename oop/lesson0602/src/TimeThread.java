import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by user on 31.08.2016.
 */
public class TimeThread implements Runnable {

    private SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");

    @Override
    public void run() {

        Thread t = Thread.currentThread();

        for (;!t.isInterrupted();) {
            System.out.println(sdf.format(new Date()));
            try {
                Thread.sleep(500);
            }
            catch (InterruptedException e) {
                return;
            }
        }
    }
}
