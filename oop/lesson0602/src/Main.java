import java.util.Random;

/**
 * Created by user on 31.08.2016.
 */
public class Main {
    public static void main(String[] args) {

        TimeThread tr1 = new TimeThread();
        Thread t1 = new Thread(tr1);
        t1.start();

        Random rn = new Random();

        int n = 0;
        for (; n != 3;) {
            n = rn.nextInt(10);
            System.out.println("n = " + n);
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
                System.out.println(e);
            }
        }
        System.out.println("TimeThread interrupted.");
        t1.interrupt();
    }
}
