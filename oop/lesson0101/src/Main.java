/**
 * Created by user on 10.08.2016.
 */
public class Main {
    public static void main(String arcv[])
    {
        System.out.println("!!!!");

        Car carOne = new Car();

        carOne.setColor("Red");
        carOne.setWeight(2000.0);
        carOne.setYear(2016);
        carOne.acceleration(100);
        carOne.deceleration(1);
        carOne.beep();
        System.out.println(carOne);
        carOne.setColor("Red");

        System.out.println("\nNEXT CAR\n");

        Car carTwo = new Car("Green", 1600.0, 2015);
        carTwo.acceleration(60);
        carTwo.beep();
        System.out.println(carTwo);

        System.out.println("\nNEXT CAR\n");

        Car car3 = new Car();
        System.out.println(car3);

        Car[] arrayCar = new Car[10];
    }
}
