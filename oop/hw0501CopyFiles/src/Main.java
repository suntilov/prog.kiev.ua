import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        String home = System.getProperty("user.home");
        try {
            FileOperation.copyFiles(new File(home + "/Downloads/"),
                                    new File(home + "/temp/"),
                                    "pdf;mp3");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
