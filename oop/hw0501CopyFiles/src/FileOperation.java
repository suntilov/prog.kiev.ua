import java.io.*;

public class FileOperation {

    final public static int defaultBufferSize = 1024 * 1024;

    private FileOperation() {
    }



    public static void copyFile(File from, File to) throws IOException {
        if (from == null || to == null) {
            throw new IllegalArgumentException("Null pointer");
        }
        try (FileInputStream fis = new FileInputStream(from);
             FileOutputStream fos = new FileOutputStream(to)) {

            byte[] buffer = new byte[defaultBufferSize];
            int byteCopy = 0;
            for (;(byteCopy = fis.read(buffer)) > 0; ) {
                fos.write(buffer, 0, byteCopy);
            }
        } catch (IOException e) {
            throw e;
        }
    }



    public static String getFileExt(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i+1);
        }
        return extension;
    }



    public static void copyFiles(File sourceFolder, File destinationFolder, String fileExtSemicolon) throws IOException {

        if (!sourceFolder.exists())
            throw new IllegalArgumentException("Source folder not exists.");

        if (!sourceFolder.isDirectory())
            throw new IllegalArgumentException("Source is not folder.");

        if (!destinationFolder.exists() && !destinationFolder.mkdir())
            throw new IOException("Cant't create destination folder.");

        File[] files = sourceFolder.listFiles(new FileFilterStr(fileExtSemicolon));

        int count = 0;
        String slash = System.getProperty("file.separator");

        for (File from : files) {
            File to = new File(destinationFolder.getAbsolutePath() + slash + from.getName());
            copyFile(from, to);
            count++;
            System.out.println(to.getAbsolutePath());
        }
        System.out.println(count + " file(s) copied.");
    }



    public static class FileFilterStr implements FileFilter {

        private String fileExtSemicolon;

        public FileFilterStr(String fileExtSemicolon) {
            this.fileExtSemicolon = fileExtSemicolon;
        }

        @Override
        public boolean accept(File pathname) {
            if (pathname.isDirectory())
                return false;
            String fileExt = getFileExt(pathname.getName());
            if (fileExt.length() > 0 && fileExtSemicolon.contains(fileExt))
                return true;
            return false;
        }
    }
}
