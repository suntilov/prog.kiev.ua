import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileOperation {

    private FileOperation() {
    }



    public static String getFileExt(String fileName) {
        int i = fileName.lastIndexOf('.');
        return (i > 0) ? fileName.substring(i+1) : "";
    }



    public static void copyFiles(File sourceFolder, File destinationFolder, String fileExtSemicolon) throws IOException {

        if (!sourceFolder.exists())
            throw new IllegalArgumentException("Source folder not exists.");
        if (!sourceFolder.isDirectory())
            throw new IllegalArgumentException("Source is not folder.");
        if (!destinationFolder.exists() && !destinationFolder.mkdir())
            throw new IOException("Cant't create destination folder.");

        File[] files = sourceFolder.listFiles(new FileFilterStr(fileExtSemicolon));

        int count = 0;
        String slash = System.getProperty("file.separator");

        int nThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(nThreads);

        for (File from : files) {
            File to = new File(destinationFolder.getAbsolutePath() + slash + from.getName());
            ThreadCopy thread = new ThreadCopy(from, to);
            executor.execute(thread);
            count++;
            System.out.println(to.getAbsolutePath());
        }

        executor.shutdown();
        while (!executor.isTerminated()) {
        }

        System.out.println(count + " file(s) copied.");
    }



    public static class FileFilterStr implements FileFilter {
        private String fileExtSemicolon;
        public FileFilterStr(String fileExtSemicolon) {
            this.fileExtSemicolon = fileExtSemicolon;
        }

        @Override
        public boolean accept(File pathname) {
            if (pathname.isDirectory())
                return false;
            String fileExt = getFileExt(pathname.getName());
            if (fileExt.length() > 0 && fileExtSemicolon.contains(fileExt))
                return true;
            return false;
        }
    }
}
