import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ThreadCopy implements Runnable {

    final public static int defaultBufferSize = 1024 * 1024;

    private File from;
    private File to;

    public ThreadCopy() {
        super();
    }

    public ThreadCopy(File from, File to) {
        super();
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        copyFile();
    }

    private void copyFile() {
        if (from == null || to == null)
            throw new IllegalArgumentException("Null pointer");
        try (FileInputStream fis = new FileInputStream(from);
             FileOutputStream fos = new FileOutputStream(to)) {
            byte[] buffer = new byte[defaultBufferSize];
            for (int byteCopy = 0; (byteCopy = fis.read(buffer)) > 0;)
                fos.write(buffer, 0, byteCopy);
        } catch (IOException e) {
            System.out.println(e.getStackTrace());
        }
    }
}
