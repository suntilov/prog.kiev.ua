import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WatchingFolder implements Runnable {

    private File folder;
    private String[] filesPrev;
    private SimpleDateFormat sdf;
    private List<String> compareResult;

    public WatchingFolder(File folder) {
        this.folder = folder;
        this.sdf = new SimpleDateFormat("hh:mm:ss");
        this.compareResult = new ArrayList<String>(10); ///< reserved for 10 changes per second
    }



    @Override
    public void run() {
        String timeStamp = sdf.format(System.currentTimeMillis());
        String[] filesNow = folder.list();
        compare(filesPrev, filesNow);
        filesPrev = filesNow;
        for (String s : compareResult) System.out.println(timeStamp + " " +s);
    }



    private void compare(String[] filesPrev, String[] filesNow) {
        compareResult.clear();
        for (String file : filesNow) {
            if (filesPrev == null)
                compareResult.add(" File is present > " + file);
            else if (!Arrays.stream(filesPrev).filter(file::equals).findFirst().isPresent())
                compareResult.add(" New file > " + file);
        }
        if (filesPrev != null)
            for (String file : filesPrev)
                if (!Arrays.stream(filesNow).filter(file::equals).findFirst().isPresent())
                    compareResult.add(" File deleted > " + file);
    }
}
