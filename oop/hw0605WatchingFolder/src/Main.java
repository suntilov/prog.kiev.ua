/**
 Реализуйте программу которая с периодичностью 1 сек,
 будет проверять состояние заданного каталога. Если в этом
 каталоге появиться новый файл или исчезнет, то выведет
 сообщение об этом событии. Программа должна работать в
 параллельном потоке выполнения.
 */

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {

        String home = System.getProperty("user.home");
        String fileSeparator = System.getProperty("file.separator");
        String folderName = home + fileSeparator + "temp";
        File folder = new File(folderName);

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        ScheduledFuture<?> res = executor.scheduleAtFixedRate(new WatchingFolder(folder), 3, 1, TimeUnit.SECONDS);
    }
}
