/**
 * Created by user on 17.08.2016.
 */
public class Main {
    public static void main(String[] args) {

        System.out.println(getMax(1,2,3,4,11,5));
        //System.out.println(getMax());
    }

    public static int getMax(int ... a) {
        int max = a[0];
        for (int i : a) {
            if (i > max)
                max = i;

        }
        return max;
    }
}
