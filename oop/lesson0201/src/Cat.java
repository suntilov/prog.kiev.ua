/**
 * Created by user on 15.08.2016.
 */
public class Cat extends Animal {
    private String name;
    private String type;

    public Cat() {
    }

    public Cat(int age, String color, boolean sex, String racion, String name, String type) {
        super(age, color, sex, racion);
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Cat{" +
                super.toString() +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public void getVoice() {
        System.out.println("May - May");
    }
}
