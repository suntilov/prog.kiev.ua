/**
 * Created by user on 15.08.2016.
 */
public class Main {
    public static void main(String argv[]) {
        HomeZoo zoo = new HomeZoo("SweetAnimalsHome");
        Animal cat = new Cat(1,"Black", true, "Banana", "Vasil", "Puh");
        zoo.addAnimal(cat);
        Animal rabbit = new Rabbit(2, "white", false, "cake", "Box", "tasty");
        zoo.addAnimal(rabbit);
        System.out.println(zoo);
    }
}
