import java.util.Arrays;

/**
 * Created by user on 15.08.2016.
 */
public class HomeZoo {
    private String name;
    private Animal[] animalArray = new Animal[10];

    public HomeZoo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean addAnimal(Animal animal) {
        for (int i = 0; i < animalArray.length; i++) {
            if (animalArray[i] == null) {
                animalArray[i] = animal;
                return true;
            }
            if (animal.getClass() == Cat.class) {
                System.out.println("Take out your cat.");
            }
            if (animal.getClass() == Rabbit.class) {
                System.out.println("It's rabbit. I'm take it for me.");
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < animalArray.length; i++) {
            sb.append(i + 1 + ") ");
            if (animalArray[i] == null)
                sb.append("free");
            else
                sb.append(animalArray[i]);
            sb.append(System.lineSeparator());
        }
        return sb.toString();
    }
}
