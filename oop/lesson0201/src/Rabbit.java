/**
 * Created by user on 15.08.2016.
 */
public class Rabbit extends Animal {
    private String name;
    private String taste;

    public Rabbit() {
    }

    public Rabbit(int age, String color, boolean sex, String racion, String name, String taste) {
        super(age, color, sex, racion);
        this.name = name;
        this.taste = taste;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    @Override
    public String toString() {
        return "Rabbit{" +
                super.toString() +
                "name='" + name + '\'' +
                ", taste='" + taste + '\'' +
                '}';
    }

    @Override
    public void getVoice() {
        System.out.println("Rab-Rab");
    }
}
