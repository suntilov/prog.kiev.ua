/**
 * Created by user on 05.09.2016.
 */
public class Main {
    public static void main(String[] args) {


        Block block = new Block();

        Thread push = new Thread(new Push(block), "Push");
        Thread pull = new Thread(new Pull(block), "Pull");

        push.start();
        pull.start();

    }
}
