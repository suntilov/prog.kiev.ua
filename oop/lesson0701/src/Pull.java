/**
 * Created by user on 05.09.2016.
 */
public class Pull implements Runnable {

    private Block block;

    public Pull(Block block) {
        this.block = block;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            if (!block.getPush()) {
                System.out.println("Pull");
                block.setPush(true);
            }
        }

    }
}
