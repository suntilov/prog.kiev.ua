/**
 * Created by user on 05.09.2016.
 */
public class Push implements Runnable {

    private Block block;

    public Push(Block block) {
        this.block = block;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            if (block.getPush()) {
                System.out.println("Push");
                block.setPush(false);
            }
        }
    }
}
