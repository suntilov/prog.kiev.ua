/**
 * Created by user on 05.09.2016.
 */
public class Block {

    boolean isPush;

    public synchronized boolean getPush() {
        try {
            wait(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return isPush;
    }

    public void setPush(boolean push) {
        isPush = push;
    }
}
