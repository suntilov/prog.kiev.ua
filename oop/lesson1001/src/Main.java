import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by user on 14.09.2016.
 */
public class Main {
    public static void main(String[] args) {

        Map<String, String> map = new HashMap<String, String>();

        try (BufferedReader f =new BufferedReader(new FileReader("map.txt"))) {
            String str="";
            for (;(str = f.readLine()) != null;) {
                String key = str.substring(0, 1);
                String value = str.substring(4);
                map.put(key, value);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }


        try (BufferedReader f =new BufferedReader(new FileReader("test.txt"))) {
            String str="";
            for (;(str = f.readLine()) != null;) {
                char[] chars = str.toCharArray();
                for (char c: chars) {
                    String s = Character.toString(c);
                    String val = map.get(s);
                    System.out.println(val);
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        task1();
    }

    public static void task1() {
        //Примеры цепочки промежуточных методов
        Map<Character, Long> ar = null;
        try {
            ar = (Map<Character, Long>) Files.lines(Paths.get("map.txt"))
                    .map(n -> n.toUpperCase())
                    .flatMapToInt(n -> n.chars())
                    .filter(n -> n>='A' && n<='Z')
                    .mapToObj((n) -> (Character.valueOf((char) n)))
                    .collect(Collectors.groupingBy(n -> n, Collectors.counting()))
                    .entrySet().stream()
                    .sorted((a, b) -> (int)(a.getValue() - b.getValue()))
                    .forEachOrdered(a -> System.out.println(a));
        } catch (IOException e) {
            System.out.println(e);
        }
        for (Character key : ar.keySet()) {
            System.out.println(key + " - " + ar.get(key));
        }
//        stream из файла
//        В верхний регистр
//        Каждую строку в отдельный поток
//        Фильтрация что символ это буква
//                Преобразование
//        Сбор к карту символ — кол-во
    }
}
